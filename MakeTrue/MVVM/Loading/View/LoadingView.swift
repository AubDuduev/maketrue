
import UIKit
import SwiftUI
import Combine

struct LoadingView: View {
  
  @State private var viewModel = LoadingViewModel()
  
  private let lottieView = GDLottieView()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: LoadingViewData
  
  var body: some View {
    
    VStack {
      self.lottieView
        .padding(.leading, 33)
        .padding(.trailing, 28)
        //.padding(.vertical, 0)
      
    }
    .onAppear(){
      self.lottieView.setupLottie(lottieFile: .logoLoad, loopMode: .autoReverse)
      self.lottieView.lottieSetup.play()
      DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        let viewData = MainTabBarViewData()
        let rootView = MainTabBarView(viewData: viewData)
        // Use a UIHostingController as window root view controller.
        UIApplication.shared.windows.first?.rootViewController = UIHostingController(rootView: rootView)
        UIApplication.shared.windows.first?.makeKey()
      }
    }
  }
}

struct LoadingPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = LoadingViewData()
    LoadingView(viewData: viewData)
  }
}
