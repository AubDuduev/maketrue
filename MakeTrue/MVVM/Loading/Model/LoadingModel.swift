
import UIKit

enum LoadingModel {
	
	case loading
	case getData
	case errorHandler(LoadingViewData?)
	case presentData(LoadingViewData)
	
	
}

