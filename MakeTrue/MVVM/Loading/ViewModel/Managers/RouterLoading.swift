
import UIKit
import Protocols

class RouterLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModel!
  
  
}
//MARK: - Initial
extension RouterLoading {
  
  //MARK: - Inition
  convenience init(viewModel: LoadingViewModel) {
    self.init()
    self.VM = viewModel
  }
}



