
import Foundation
import Protocols
import SwiftUI
import Combine

class LoadingViewModel: VMManagers {
	
	public var LoadingModel: LoadingModel = .loading {
		didSet{
			self.commonLogicLoading()
		}
	}
  
  //MARK: - Public variable
  public var managers    : LoadingManagers!
  public var VC          : LoadingViewController!
  public var view        : LoadingView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicLoading(){
    
    switch self.LoadingModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = LoadingManagers(viewModel: self)
  }
}
//MARK: - Initial
extension LoadingViewModel {
  
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LoadingManagers(viewModel: self)
  }
}
