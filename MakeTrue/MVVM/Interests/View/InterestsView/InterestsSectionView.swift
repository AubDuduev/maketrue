
import SwiftUI

struct InterestsSectionView: View {
  
  public let sectionData: SectionData!
  
  @Binding var selections: [UUID]
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      ZStack {
        Rectangle()
          .frame(width: commonSize.width, height: commonSize.height)
          .foregroundColor(.white)
        Image.set(self.sectionData.imageSection)
          .resizable()
          .padding(.horizontal, 16)
          .padding(.vertical, 5)
        Text(sectionData.nameSection)
          .font(Font.system(size: 14))
        
      }
      .onTapGesture {
        if self.selections.contains(sectionData.id) {
          let firstIndex = self.selections.firstIndex(of: sectionData.id)
          self.selections.remove(at: firstIndex!)
        } else {
          self.selections.append(sectionData.id)
        }
      }
    }
    .frame(height: 72)
  }
}

