
import UIKit
import SwiftUI
import Combine

struct InterestsView: View {
  
  @ObservedObject private var viewModel = InterestsViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: InterestsViewData
  
  @State var selections = [UUID]()
  
  var body: some View {
    
    GeometryReader { geo in
      let commonSize = geo.size
      
      ScrollView {
        VStack {
          ForEach(viewData.data, id: \.id) { sectionData in
            //Create sections
            
            InterestsSectionView(sectionData: sectionData,
                                 selections: self.$selections)
            VStack {
              //Create rows
              if  selections.contains(sectionData.id) {
                ForEach(0..<sectionData.dataSection.count) { row in
                  InterestsRowView(name: sectionData.dataSection[row])
                    .environmentObject(self.viewModel)
                }//end forEach cell
                .frame(width    : commonSize.width,
                       height   : 20,
                       alignment: .center)
              }
            }//end vstack rows
          }
          
          Spacer()
        }//end List
        .animation(.easeInOut(duration: 0.4))
        .background(Color.white)
        .frame(width: commonSize.width, height: commonSize.height)
      }
    }
    .navigationBarTitle(Text.set(.navBarInterests), displayMode: .inline)
    .backButton()
    .navigationBarColor(bg: .white, title: .set(.blueProject))
    
    
  }
  
}

struct InterestsPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = InterestsViewData()
    InterestsView(viewData: viewData)
  }
}
