
import SwiftUI

struct InterestsRowView: View {
  
  public let name: String!
  
  @EnvironmentObject private var viewModel: InterestsViewModel
  
  @State var isCheckmark = false
  @State public var lottieView  = GDLottieView()
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      
      HStack {
        lottieView
          .frame(width    : commonSize.height,
                 height   : commonSize.height,
                 alignment: .center)
        Text(self.name)
          .font(Font.set(.latoRegular, size: 14))
      }
      
      .padding(.horizontal, 25)
      .frame(width    : commonSize.width,
             height   : commonSize.height,
             alignment: .leading)
      .onAppear{
        self.viewModel.managers.setup.lottieView(view: self)
      }
      .onTapGesture {
        self.viewModel.managers.animation.subscribeAnimation(view: self)
      }
    }
  }
}

struct InterestsRowView_Previews: PreviewProvider {
  static var previews: some View {
    InterestsRowView(name: "")
      .previewLayout(.fixed(width: 300, height: 20))
  }
}
