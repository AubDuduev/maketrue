import UIKit

class InterestsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: InterestsViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = InterestsViewModel(viewController: self)
  }
}
