
import UIKit
import SwiftUI

struct InterestsViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let InterestsVC = InterestsViewController()
    return InterestsVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
