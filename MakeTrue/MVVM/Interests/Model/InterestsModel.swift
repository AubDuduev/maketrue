
import UIKit

enum InterestsModel {
	
	case loading
	case getData
	case errorHandler(InterestsViewData?)
	case presentData(InterestsViewData)
	
	
}

