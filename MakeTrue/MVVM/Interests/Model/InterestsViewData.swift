
import UIKit
import Combine
import SwiftUI

class InterestsViewData: ObservableObject {
  
  @Published var data = [SectionData(nameSection: "Еда и Напитки",
                                    imageSection: .sectionInterestsOne,
                                    dataSection: ["Рестораны", "Кофейни", "Рецепты"]),
                         
                         SectionData(nameSection: "Здоровье и Красота",
                                    imageSection: .sectionInterestsTwo,
                                    dataSection: ["Фитнес", "Косметика"]),
                         
                         SectionData(nameSection: "Мода и Стиль",
                                    imageSection: .sectionInterestsThree,
                                    dataSection: ["Фото и Видео", "Распродажи", "Аксессуары", "Одежда"])]
}

struct SectionData: Identifiable {
  
  
  var id = UUID()

  let nameSection : String!
  let imageSection: Image.ImageNames!
  let dataSection : [String]!
  
}
