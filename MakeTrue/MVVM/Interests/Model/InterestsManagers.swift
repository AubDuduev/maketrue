
import Foundation
import Protocols

class InterestsManagers: VMManagers {
  
  let setup    : SetupInterests!
  let server   : ServerInterests!
  let present  : PresentInterests!
  let logic    : LogicInterests!
  let animation: AnimationInterests!
  let router   : RouterInterests!
  
  init(viewModel: InterestsViewModel) {
    
    self.setup     = SetupInterests(viewModel: viewModel)
    self.server    = ServerInterests(viewModel: viewModel)
    self.present   = PresentInterests(viewModel: viewModel)
    self.logic     = LogicInterests(viewModel: viewModel)
    self.animation = AnimationInterests(viewModel: viewModel)
    self.router    = RouterInterests(viewModel: viewModel)
  }
}

