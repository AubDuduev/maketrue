
import Foundation
import Protocols
import SwiftUI
import Combine

class InterestsViewModel: VMManagers, ObservableObject {
	
	public var InterestsModel: InterestsModel = .loading {
		didSet{
			self.commonLogicInterests()
		}
	}
  
  //MARK: - Public variable
  public var managers    : InterestsManagers!
  public var VC          : InterestsViewController!
  public var view        : InterestsView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicInterests(){
    
    switch self.InterestsModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = InterestsManagers(viewModel: self)
  }
}
//MARK: - Initial
extension InterestsViewModel {
  
  convenience init(viewController: InterestsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = InterestsManagers(viewModel: self)
  }
}
