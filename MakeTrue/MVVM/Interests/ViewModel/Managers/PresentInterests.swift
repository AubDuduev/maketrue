
import UIKit
import Protocols

class PresentInterests: VMManager {
  
  //MARK: - Public variable
  public var VM: InterestsViewModel!
  
  
}
//MARK: - Initial
extension PresentInterests {
  
  //MARK: - Inition
  convenience init(viewModel: InterestsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

