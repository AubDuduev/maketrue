import UIKit
import Protocols

class AnimationInterests: VMManager {
  
  //MARK: - Public variable
  public var VM: InterestsViewModel!
  
  public func subscribeAnimation(view: InterestsRowView){
    if view.isCheckmark {
      view.lottieView.lottieSetup.playProgress(fromProgress: 0.2, toProgress: 1.0)
    } else {
      view.lottieView.lottieSetup.playProgress(fromProgress: 0.0, toProgress: 0.2)
    }
    view.isCheckmark.toggle()
  }
}
//MARK: - Initial
extension AnimationInterests {
  
  //MARK: - Inition
  convenience init(viewModel: InterestsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

