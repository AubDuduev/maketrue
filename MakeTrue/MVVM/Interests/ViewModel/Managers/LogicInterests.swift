import Foundation
import Protocols

class LogicInterests: VMManager {
  
  //MARK: - Public variable
  public var VM: InterestsViewModel!
  
  
}
//MARK: - Initial
extension LogicInterests {
  
  //MARK: - Inition
  convenience init(viewModel: InterestsViewModel) {
    self.init()
    self.VM = viewModel
  }
}
