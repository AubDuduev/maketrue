
import Foundation
import Protocols

class SettingManagers: VMManagers {
  
  let setup    : SetupSetting!
  let server   : ServerSetting!
  let present  : PresentSetting!
  let logic    : LogicSetting!
  let animation: AnimationSetting!
  let router   : RouterSetting!
  
  init(viewModel: SettingViewModel) {
    
    self.setup     = SetupSetting(viewModel: viewModel)
    self.server    = ServerSetting(viewModel: viewModel)
    self.present   = PresentSetting(viewModel: viewModel)
    self.logic     = LogicSetting(viewModel: viewModel)
    self.animation = AnimationSetting(viewModel: viewModel)
    self.router    = RouterSetting(viewModel: viewModel)
  }
}

