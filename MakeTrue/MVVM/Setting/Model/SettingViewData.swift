
import UIKit
import Combine

class SettingViewData: ObservableObject {
  
  @Published var name   = "Aleksandr"
  @Published var numberNumber = "+7999333557"
  
  
  
  /// ddsdsdd
  public var settingDates: [SettingData] {
    get {
      var settingDates = [SettingData]()
      for index in 0..<SettingSectionName.allCases.count {
        let name = SettingSectionName.allCases[index]
        let icon = SettingSectionIcon.allCases[index]
        let settingData = SettingData(id: UUID(), name: name, icon: icon)
        settingDates.append(settingData)
      }
    return settingDates
    }
  }
}

struct SettingData: Hashable, Identifiable {
  
  var id = UUID()
  let name: SettingSectionName!
  let icon: SettingSectionIcon!
}
enum SettingSectionName: String, CaseIterable {

  case language  = "Язык:"
  case delete    = "Удалить аккаунт"
}

enum SettingSectionIcon: String, CaseIterable {
  
  case arrowLanguageSetting
  case deleteSetting
}
