
import UIKit
import Foundation

class SettingSetConstraint {
  
  
  public func setConstraint() -> SettingConstraintoble {
    print(GDScreenDevice.get().rawValue, "current device")
    
    switch GDScreenDevice.get() {
      
      //1 - iPhone 5
      case .iPhone5:
        return iPhone5SettingConstraints()
      //2 - iPhone 7
      case .iPhone7:
        return iPhone7SettingConstraints()
      //3 - iPhone Plus
      case .iPhonePlus:
        return iPhonePlusSettingConstraints()
      //4 - iPhone X
      case .iPhoneX:
        return iPhoneXSettingConstraints()
      //5 - iPhone 12
      case .iPhone12:
        return iPhone12SettingConstraints()
      //6 - iPhone MAX
      case .iPhoneMax:
        return iPhoneMaxSettingConstraints()
      //7 - iPhone 12 MAX
      case .iPhone12Max:
        return iPhone12MaxSettingConstraints()
    }
  }
}




