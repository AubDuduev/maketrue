
import UIKit

enum SettingModel {
	
	case loading
	case getData
	case errorHandler(SettingViewData?)
	case presentData(SettingViewData)
	
	
}

