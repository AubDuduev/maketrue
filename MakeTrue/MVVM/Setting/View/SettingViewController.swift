import UIKit

class SettingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: SettingViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = SettingViewModel(viewController: self)
  }
}
