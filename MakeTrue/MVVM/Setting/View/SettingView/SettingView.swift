
import UIKit
import SwiftUI
import Combine

struct SettingView: View {
  
  @State private var viewModel = SettingViewModel()
  @Environment (\.presentationMode) var dismiss
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData = SettingViewData()
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack(spacing: 0){
        //MARK: - SettingLanguageView
        SettingLanguageView(settingData: viewData.settingDates[0])
          .environmentObject(self.viewModel)
        
        //MARK: - SettingDeleteAccountView
        SettingDeleteAccountView(settingData: viewData.settingDates[1])
          .environmentObject(self.viewModel)
        Spacer()
      }
      .frame(width: commonSize.width, height: commonSize.height, alignment: .center)
     
    }
    .navigationBarTitle(Text.set(.navBarSetting), displayMode: .inline)
    .backButton()
    .navigationBarColor(bg: .white, title: .set(.blueProject))
    
  }
}

struct SettingPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = SettingViewData()
    SettingView(viewData: viewData)
  }
}
