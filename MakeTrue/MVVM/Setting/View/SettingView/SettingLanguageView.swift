
import UIKit
import SwiftUI

struct SettingLanguageView: View {
  
  public var settingData: SettingData
  
  @EnvironmentObject public var viewModel: SettingViewModel
  
  @State public var alertIsPresented = false
  @State public var currentLanguage  = "Русский"
  
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack(spacing: -5){
        HStack(spacing: 10) {
          //MARK: - Name
          Button(action: {
            self.alertIsPresented.toggle()
          }, label: {
            Text(self.settingData.name.rawValue)
              .font(Font.set(.latoRegular, size: 14.5))
              .foregroundColor(.set(.settingButton))
              .padding(.top, 19)
              .padding(.bottom, 13)
          })
          //MARK: - Text currentLanguage
          Text(self.currentLanguage)
            .font(Font.set(.latoRegular, size: 14.5))
            .foregroundColor(.set(.settingButton))
            .padding(.bottom, 14)
            .padding(.top, 22)
            .onTapGesture {
              self.alertIsPresented.toggle()
            }
          //MARK: - Spacer
          Spacer()
          //MARK: - Arrow right
          Button(action: {
            self.alertIsPresented.toggle()
          }, label: {
            Image.set(self.settingData.icon.rawValue)
              .resizable()
              .renderingMode(.template)
              .foregroundColor(.set(.settingButton))
              .aspectRatio(contentMode: .fit)
              .padding(.bottom, 14)
              .padding(.top, 22)
              .frame(width : commonSize.height,
                     height: commonSize.height,
                     alignment: .center)
          })
        }
        .padding(.leading, 15)
        .alertSheet(.information, .changeToLanguage, .changeToLanguage, self.$alertIsPresented) { (response) in
          self.viewModel.managers.logic.alertSheet(view: self, response: response)
        }
        //MARK: - Divider
        Divider().frame(width: commonSize.width - 32, height: 1, alignment: .center)
      }
      .frame(width    : commonSize.width,
             height   : commonSize.height,
             alignment: .center)
    }
    .frame(height: 55, alignment: .center)
  }
}

struct SettingLanguageViewPreviews: PreviewProvider {
  static var previews: some View {
    let settingData = SettingData(name: .language, icon: .arrowLanguageSetting)
    SettingLanguageView(settingData: settingData)
      .environmentObject(SettingViewModel())
      .previewLayout(.fixed(width: 315, height: 55))
  }
}
