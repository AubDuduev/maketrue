
import UIKit
import SwiftUI

struct SettingDeleteAccountView: View {
  
  public var settingData: SettingData
  
  @EnvironmentObject public var viewModel: SettingViewModel
  
  @State public var alertIsPresented = false
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack(spacing: -5) {
        HStack(spacing: 10) {
          //MARK: - Name
          Button(action: {
            self.alertIsPresented.toggle()
          }, label: {
            Text(self.settingData.name.rawValue)
              .font(Font.set(.latoRegular, size: 14.5))
              .foregroundColor(.set(.settingButton))
              .padding(.top, 19)
              .padding(.bottom, 13)
          })
          //MARK: - Spacer
          Spacer()
          //MARK: - Arrow right
          Button(action: {
            self.alertIsPresented.toggle()
          }, label: {
            Image.set(self.settingData.icon.rawValue)
              .resizable()
              .renderingMode(.template)
              .foregroundColor(.set(.settingButton))
              .aspectRatio(contentMode: .fit)
              .padding(.bottom, 14)
              .padding(.top, 22)
              .frame(width : commonSize.height,
                     height: commonSize.height,
                     alignment: .center)
          })
        }
        .padding(.leading, 15)
        .alertSheet(.information, .deleteToAccount, .deleteToAccount, self.$alertIsPresented, completion: { (responce) in
          
        })
        //MARK: - Divider
        Divider().frame(width: commonSize.width - 32, height: 1, alignment: .center)
      }
      .frame(width    : commonSize.width,
             height   : commonSize.height,
             alignment: .center)
    }
    .frame(height: 55, alignment: .center)
  }
}

struct SettingDeleteAccountPreviews: PreviewProvider {
  static var previews: some View {
    let settingData = SettingData(name: .delete, icon: .deleteSetting)
    SettingDeleteAccountView(settingData: settingData)
      .environmentObject(SettingViewModel())
      .previewLayout(.fixed(width: 315, height: 55))
  }
}
