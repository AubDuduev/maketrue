
import UIKit
import Protocols
import SwiftUI
//import SnapKit


class SetupSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModel!
  
  public func hostingVC(){
//    let view     = SettingView(viewData: self.VM.viewData)
//    let hostingVC = UIHostingController(rootView: view)
//    self.VM.VC.view.addSubview(hostingVC.view)
//    self.VM.VC.addChild(hostingVC)
//    hostingVC.view.snp.makeConstraints { (hostingVCView) in
//      hostingVCView.edges.equalTo(self.VM.VC.view)
//    }
  }
  public func cancellableViewData(){
//    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
//      print(name)
//    })
  }
  public func choiceDevice() -> SettingConstraintoble {
    let setConstraint = SettingSetConstraint()
    return setConstraint.setConstraint()
  }
}
//MARK: - Initial
extension SetupSetting {
  
  //MARK: - Inition
  convenience init(viewModel: SettingViewModel) {
    self.init()
    self.VM = viewModel
  }
}


