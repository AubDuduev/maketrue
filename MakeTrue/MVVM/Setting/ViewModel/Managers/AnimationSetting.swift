import UIKit
import Protocols

class AnimationSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModel!
  
  
}
//MARK: - Initial
extension AnimationSetting {
  
  //MARK: - Inition
  convenience init(viewModel: SettingViewModel) {
    self.init()
    self.VM = viewModel
  }
}

