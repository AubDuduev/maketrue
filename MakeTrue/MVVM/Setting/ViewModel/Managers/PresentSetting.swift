
import UIKit
import Protocols

class PresentSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModel!
  
  
}
//MARK: - Initial
extension PresentSetting {
  
  //MARK: - Inition
  convenience init(viewModel: SettingViewModel) {
    self.init()
    self.VM = viewModel
  }
}

