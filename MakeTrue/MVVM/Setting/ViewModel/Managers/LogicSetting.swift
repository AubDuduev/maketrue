import Foundation
import Protocols

class LogicSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModel!
  
  public func alertSheet(view: SettingLanguageView, response: Int){
    
    switch response {
      case 0:
        view.currentLanguage = "Русский"
      case 1:
        view.currentLanguage = "Английский"
      default:
        break
    
    }
  }
}
//MARK: - Initial
extension LogicSetting {
  
  //MARK: - Inition
  convenience init(viewModel: SettingViewModel) {
    self.init()
    self.VM = viewModel
  }
}
