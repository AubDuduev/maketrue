
import Foundation
import Protocols
import SwiftUI
import Combine

class SettingViewModel: VMManagers, ObservableObject {
	
	public var SettingModel: SettingModel = .loading {
		didSet{
			self.commonLogicSetting()
		}
	}
  
  //MARK: - Public variable
  public var managers    : SettingManagers!
  public var VC          : SettingViewController!
  public var view        : SettingView!
  public var cancellable : AnyCancellable!
  public var constraints : SettingConstraintoble {
    get {
      return self.managers.setup.choiceDevice()
    }
  }
  
  public func commonLogicSetting(){
    
    switch self.SettingModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = SettingManagers(viewModel: self)
  }
}
//MARK: - Initial
extension SettingViewModel {
  
  convenience init(viewController: SettingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = SettingManagers(viewModel: self)
  }
}
