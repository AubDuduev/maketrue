import UIKit

class iPhone7BrandCardConstraints: BrandCardConstraintoble {
  
  //BCHeaderView
  var headerViewHeight         : CGFloat = 314
  //BCCouponeView
  var couponeViewHeight        : CGFloat = 99
  var couponeViewPaddingV      : CGFloat = 18
  var couponeViewPaddingH      : CGFloat = 13
  //BrandContactButtonsView
  var contactsButtonsViewHeight: CGFloat = 99
  var contactsSquare           : CGFloat = 54
  var contactsViewPaddingV     : CGFloat = 22
  var contactsViewPaddingH     : CGFloat = 38
  //BCDescriptionView
  var descriptionViewHeight    : CGFloat = 99
  var descriptionTextPaddingV  : CGFloat = 18
  var descriptionTextPaddingH  : CGFloat = 13
  //BCMenuView
  var menuViewHeight           : CGFloat = 99
  var menuViewPaddingV         : CGFloat = 18
  var menuViewPaddingH         : CGFloat = 13
  //BCOpeningHours
  var openingHoursHeight       : CGFloat = 99
  var openingPaddingH          : CGFloat = 16
  var openingPaddingT          : CGFloat = 13
  //BCTagsView
  var tagsViewHeight           : CGFloat = 99
  var tagsPaddingH             : CGFloat = 16
  var tagsPaddingT             : CGFloat = 13
  //BCNewsView
  var newsViewHeight           : CGFloat = 442
  var newsPaddingH             : CGFloat = 16
  var newsPaddingT             : CGFloat = 13
  var newScrollPaddingL        : CGFloat = 15
  var newScrollPaddingB        : CGFloat = 10
  var newViewWidth             : CGFloat = 309
  var newViewHeight            : CGFloat = 355
  var newTimeViewHeight        : CGFloat = 20
  var newImageViewHeight       : CGFloat = 163
  var newDataViewHeight        : CGFloat = 171
  //BCGalleryView
  var galleryViewHeight        : CGFloat = 369.79
  var galleryViewWidth         : CGFloat = 375
  var galleryMainPhotoWidth    : CGFloat = 346.01
  var galleryMainPhotoHeight   : CGFloat = 200.32
  var galleryPhotoViewWidth    : CGFloat = 66
  var galleryPhotoViewHeight   : CGFloat = 66
  var galleryPaddingH          : CGFloat = 16
  var galleryPaddingT          : CGFloat = 13
  var galleryPhotoPaddingV     : CGFloat = 5
  var galleryPhotoPaddingL     : CGFloat = 10
  //BCMapView
  var mapViewHeight            : CGFloat = 373
  var mapPaddingH              : CGFloat = 16
  var mapPaddingT              : CGFloat = 13
  var mapPaddingB              : CGFloat = 85
  var gdMapViewWidth           : CGFloat = 345.11
  var gdMapViewHeight          : CGFloat = 188.41
}
