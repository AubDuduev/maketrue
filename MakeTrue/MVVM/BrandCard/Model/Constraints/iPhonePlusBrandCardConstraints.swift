import UIKit

class iPhonePlusBrandCardConstraints: BrandCardConstraintoble {
  
  //BCHeaderView
  var headerViewHeight         : CGFloat = 314
  //BCCouponeView
  var couponeViewHeight        : CGFloat = 108
  var couponeViewPaddingV      : CGFloat = 21
  var couponeViewPaddingH      : CGFloat = 16
  //BrandContactButtonsView
  var contactsButtonsViewHeight: CGFloat = 109
  var contactsSquare           : CGFloat = 60
  var contactsViewPaddingV     : CGFloat = 25
  var contactsViewPaddingH     : CGFloat = 42
  //BCDescriptionView
  var descriptionViewHeight    : CGFloat = 109
  var descriptionTextPaddingV  : CGFloat = 13
  var descriptionTextPaddingH  : CGFloat = 15
  //BCMenuView
  var menuViewHeight           : CGFloat = 109
  var menuViewPaddingV         : CGFloat = 26
  var menuViewPaddingH         : CGFloat = 16
  //BCOpeningHours
  var openingHoursHeight       : CGFloat = 109
  var openingPaddingH          : CGFloat = 16
  var openingPaddingT          : CGFloat = 13
  //BCTagsView
  var tagsViewHeight           : CGFloat = 109
  var tagsPaddingH             : CGFloat = 16
  var tagsPaddingT             : CGFloat = 13
  //BCNewsView
  var newsViewHeight           : CGFloat = 489
  var newsPaddingH             : CGFloat = 16
  var newsPaddingT             : CGFloat = 13
  var newScrollPaddingL        : CGFloat = 15
  var newScrollPaddingB        : CGFloat = 20
  var newViewWidth             : CGFloat = 342
  var newViewHeight            : CGFloat = 370
  var newTimeViewHeight        : CGFloat = 20
  var newImageViewHeight       : CGFloat = 182
  var newDataViewHeight        : CGFloat = 189
  //BCGalleryView
  var galleryViewHeight        : CGFloat = 408
  var galleryViewWidth         : CGFloat = 414
  var galleryMainPhotoWidth    : CGFloat = 382
  var galleryMainPhotoHeight   : CGFloat = 221
  var galleryPhotoViewWidth    : CGFloat = 73
  var galleryPhotoViewHeight   : CGFloat = 73
  var galleryPaddingH          : CGFloat = 16
  var galleryPaddingT          : CGFloat = 13
  var galleryPhotoPaddingV     : CGFloat = 5
  var galleryPhotoPaddingL     : CGFloat = 10
  //BCMapView
  var mapViewHeight            : CGFloat = 340
  var mapPaddingH              : CGFloat = 16
  var mapPaddingT              : CGFloat = 13
  var mapPaddingB              : CGFloat = 35
  var gdMapViewWidth           : CGFloat = 381
  var gdMapViewHeight          : CGFloat = 208
}
