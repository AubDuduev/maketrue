import UIKit

class iPhone12BrandCardConstraints: BrandCardConstraintoble {
  
  //BCHeaderView
  var headerViewHeight         : CGFloat = 295
  //BCCouponeView
  var couponeViewHeight        : CGFloat = 101
  var couponeViewPaddingV      : CGFloat = 18
  var couponeViewPaddingH      : CGFloat = 16
  //BrandContactButtonsView
  var contactsButtonsViewHeight: CGFloat = 101
  var contactsSquare           : CGFloat = 56
  var contactsViewPaddingV     : CGFloat = 22
  var contactsViewPaddingH     : CGFloat = 39
  //BCDescriptionView
  var descriptionViewHeight    : CGFloat = 101
  var descriptionTextPaddingV  : CGFloat = 18
  var descriptionTextPaddingH  : CGFloat = 16
  //BCMenuView
  var menuViewHeight           : CGFloat = 101
  var menuViewPaddingV         : CGFloat = 18
  var menuViewPaddingH         : CGFloat = 16
  //BCOpeningHours
  var openingHoursHeight       : CGFloat = 102
  var openingPaddingH          : CGFloat = 16
  var openingPaddingT          : CGFloat = 13
  //BCTagsView
  var tagsViewHeight           : CGFloat = 102
  var tagsPaddingH             : CGFloat = 16
  var tagsPaddingT             : CGFloat = 13
  //BCNewsView
  var newsViewHeight           : CGFloat = 460
  var newsPaddingH             : CGFloat = 16
  var newsPaddingT             : CGFloat = 13
  var newScrollPaddingL        : CGFloat = 16
  var newScrollPaddingB        : CGFloat = 20
  var newViewWidth             : CGFloat = 321
  var newViewHeight            : CGFloat = 368
  var newTimeViewHeight        : CGFloat = 20
  var newImageViewHeight       : CGFloat = 170
  var newDataViewHeight        : CGFloat = 178
  //BCGalleryView
  var galleryViewHeight        : CGFloat = 384.5
  var galleryViewWidth         : CGFloat = 390
  var galleryMainPhotoWidth    : CGFloat = 360
  var galleryMainPhotoHeight   : CGFloat = 208
  var galleryPhotoViewWidth    : CGFloat = 68
  var galleryPhotoViewHeight   : CGFloat = 68
  var galleryPaddingH          : CGFloat = 16
  var galleryPaddingT          : CGFloat = 13
  var galleryPhotoPaddingV     : CGFloat = 5
  var galleryPhotoPaddingL     : CGFloat = 10
  //BCMapView
  var mapViewHeight            : CGFloat = 347
  var mapPaddingH              : CGFloat = 16
  var mapPaddingT              : CGFloat = 13
  var mapPaddingB              : CGFloat = 50
  var gdMapViewWidth           : CGFloat = 360
  var gdMapViewHeight          : CGFloat = 195
}
