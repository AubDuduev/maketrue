
import Foundation
import Protocols

class BrandCardManagers: VMManagers {
  
  let setup    : SetupBrandCard!
  let server   : ServerBrandCard!
  let present  : PresentBrandCard!
  let logic    : LogicBrandCard!
  let animation: AnimationBrandCard!
  let router   : RouterBrandCard!
  
  init(viewModel: BrandCardViewModel) {
    
    self.setup     = SetupBrandCard(viewModel: viewModel)
    self.server    = ServerBrandCard(viewModel: viewModel)
    self.present   = PresentBrandCard(viewModel: viewModel)
    self.logic     = LogicBrandCard(viewModel: viewModel)
    self.animation = AnimationBrandCard(viewModel: viewModel)
    self.router    = RouterBrandCard(viewModel: viewModel)
  }
}

