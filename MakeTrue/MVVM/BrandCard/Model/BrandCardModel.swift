
import UIKit

enum BrandCardModel {
	
	case loading
	case getData
	case errorHandler(BrandCardViewData?)
	case presentData(BrandCardViewData)
	
	
}

