
import UIKit
import SwiftUI

struct ActionRestarauntView: View {
  
  @State public var isPresentBackgraund = false
  @State public var opacity = 0.0
  
  public let viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      ZStack {
        Rectangle().foregroundColor(.set(.balckOpacity))
          .opacity(self.opacity)
        
        ActionButtonView(index: 0)
          .offset(x: (commonSize.center(.right)  - (56 / 2)) - 64,
                  y: (commonSize.center(.bottom) - (56 / 2)) - 193)
          .opacity(self.opacity)
        
        ActionButtonView(index: 1)
          .offset(x: (commonSize.center(.right)  - (56 / 2)) - 135,
                  y: (commonSize.center(.bottom) - (56 / 2)) - 92)
          .opacity(self.opacity)
          
        ZStack {
          Circle()
            .foregroundColor(.set(.blueProject))
            .frame(width: 78, height: 78, alignment: .center)
          Button(action: {
            self.viewModel.managers.animation.actionsAnimation(view: self)
          }, label: {
            Image.set(.actionRestourantButton)
              .resizable()
              .renderingMode(.template)
              .foregroundColor(.white)
              .frame(width: 42, height: 42, alignment: .center)
              .offset(x: -1, y: 0)
          })
          .frame(width: 42, height: 42, alignment: .center)
        }
        .shadow(corner: 42, radius: 10, color: .set(.shadowBC))
        .offset(x: (commonSize.center(.right) - (78 / 2)) - 17,
                y: (commonSize.center(.bottom) - (78 / 2)) - 80)
      }
    }
  }
}

struct ActionRestarauntViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    ActionRestarauntView(viewModel: viewModel)
  }
}

