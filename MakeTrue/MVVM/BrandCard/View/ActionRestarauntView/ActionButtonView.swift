
import SwiftUI

struct ActionButtonView: View {
  
  public let index: Int
  
  var body: some View {
    VStack(spacing: 5) {
      ZStack {
        Circle()
          .foregroundColor(.set(.orangeProject))
          .frame(width: 56, height: 56, alignment: .center)
        Image.set(ActionButtonImage.allCases[index].rawValue)
          .resizable()
          .renderingMode(.template)
          .foregroundColor(.white)
          .aspectRatio(contentMode: .fit)
          .frame(width: 36, height: 36, alignment: .center)
          
      }
      .shadow(corner: 42, radius: 5, color: .set(.shadowBC))
      Text(ActionButtonName.allCases[index].rawValue)
        .font(Font.set(.latoRegular, size: 14))
        .padding(5)
        .background(Color.white)
        .cornerRadius(4)
        .offset(x: -20, y: 0)
    }
  }
}

struct OrderTableView_Previews: PreviewProvider {
  static var previews: some View {
    ActionButtonView(index: 0)
  }
}

enum ActionButtonName: String, CaseIterable {
  case orderTable    = "Забронировать столик"
  case orderDilevery = "Заказать доставку"
}

enum ActionButtonImage: String, CaseIterable {
  case orderTable    = "orderTableBarndCard"
  case orderDilevery = "orderDileveryBarndCard"
}
