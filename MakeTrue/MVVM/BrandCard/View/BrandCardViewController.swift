import UIKit

class BrandCardViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: BrandCardViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = BrandCardViewModel(viewController: self)
  }
}
