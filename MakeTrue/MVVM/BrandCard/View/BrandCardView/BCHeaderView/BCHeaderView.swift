
import SwiftUI

struct BCHeaderView: View {
  
  @Environment(\.presentationMode) var dissmiss
  
  public var viewModel: BrandCardViewModel!
  
  @State public var lottieView  = GDLottieView()
  @State public var isSubscribe = false
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize  = geo.size
      
      ZStack {
        //MARK: - Image preview
        ZStack {
          Image.set(.brandCardHeader)
            .resizable()
            .aspectRatio(contentMode: .fill)
          Rectangle().foregroundColor(Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)))
            .frame(width: commonSize.width)
        }
        
        //MARK: - Common VStack
        VStack {
          Spacer(minLength: 38)
          //MARK: - Left arrow button and Right like button
          HStack {
            //MARK: - Left arrow button
            Button(action: { self.dissmiss.wrappedValue.dismiss() }, label: {
              Image.set(.arrowLeftBC)
                .resizable()
                .frame(width: 15, height: 15)
            })
            
            .frame(width: 36, height: 36)
            .background(RoundedRectangle(cornerRadius: 18.5).foregroundColor(.white))
            Spacer()
            //MARK: - Right like button
            lottieView
              .frame(width: 36, height: 36)
              .background(RoundedRectangle(cornerRadius: 18.5).foregroundColor(.white))
              .onAppear{
                self.viewModel.managers.setup.lottieView(view: self)
              }
              .onTapGesture {
                self.viewModel.managers.animation.subscribeAnimation(view: self)
              }
          }
          .padding(.horizontal, 18)
          .frame(height: 36)
          
          //MARK: - Name brand
          Spacer(minLength: 54)
          VStack {
            HStack {
              Text("ТРЦ")
                .font(Font.set(.latoRegular, size: 28))
                .fontWeight(.semibold)
                .foregroundColor(.white)
              Spacer()
            }
            HStack {
              Text("«Европейский»")
                .font(Font.set(.LatoBlack, size: 28))
                .font(.title)
                .foregroundColor(.white)
              Spacer()
            }
          }
          .padding(.horizontal, 18)
          .frame(alignment: .leading)
          
          //MARK: - Metro pin - Distance pin
          Spacer(minLength: 58)
          VStack {
            //MARK: - Metro pin
            HStack {
              Circle()
                .foregroundColor(.red)
                .frame(width: 11, height: 11)
              Text("м. Киевская")
                .font(Font.set(.latoRegular, size: 14))
                .fontWeight(.semibold)
                .foregroundColor(.white)
              Spacer()
            }
            //MARK: - Distance pin
            HStack {
              Image.set(.metroBrandCard)
                .resizable()
                .renderingMode(.template)
                .foregroundColor(.white)
                .aspectRatio(contentMode: .fit)
                .frame(width: 15, height: 15)
                .offset(x: -2, y: 0)
              Text("~15 км")
                .font(Font.set(.latoRegular, size: 14))
                .fontWeight(.semibold)
                .foregroundColor(.white)
              Spacer()
            }
          }
          .padding(.horizontal, 18)
          
          Spacer(minLength: 10)
          
        }//end common vstack
        .modifier(FrameStretch(geo: geo))
        
      }//end ZStack
      .modifier(OffsetStretch(geo: geo))
      .modifier(FrameStretch(geo: geo))
      
    }
  }
}

struct BCHeaderViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCHeaderView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 314))
  }
}
