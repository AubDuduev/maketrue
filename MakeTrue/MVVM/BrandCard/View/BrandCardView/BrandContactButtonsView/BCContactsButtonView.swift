
import UIKit
import SwiftUI

struct BCContactsButtonView: View {
  
  public let index    : Int
  public var viewModel: BrandCardViewModel!
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      ZStack {
        //Fon white
        RoundedRectangle(cornerRadius: 8)
          .frame(width : viewModel.constraints.contactsSquare,
                 height: viewModel.constraints.contactsSquare)
          .foregroundColor(.white)
          .shadow(color: .set(.shadowButtonContactBC), radius: 4)
        VStack(spacing: 3){
          //Icon
          Image.set(ImageButtons.allCases[self.index].rawValue)
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 16, height: 16)
          
          //Text
          Text(NameButtons.allCases[self.index].rawValue)
            .frame(width: 45, height: 13)
            .font(Font.set(.latoRegular, size: 10))
          
        }.frame(width: commonSize.width, height: commonSize.height, alignment: .center)
      }.frame(width: commonSize.width, height: commonSize.height, alignment: .center)
    }
  }
}
struct BrandContactButtonViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCContactsButtonView(index: 0, viewModel: viewModel)
      .previewDevice("iPhone 11")
      .previewDisplayName("Preview")
      .previewLayout(.fixed(width: 54, height: 54))
  }
}

enum NameButtons: String, CaseIterable {
  
  case phone   = "Телефон"
  case address = "Адрес"
  case site    = "Саит"
  case email   = "Почта"
}
enum ImageButtons: String, CaseIterable {
  
  case emailBrandCard
  case phoneBrandCard
  case mapBrandCard
  case websiteBrandCard
}
