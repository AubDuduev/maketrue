
import SwiftUI

struct BCContactsButtonsView: View {
  
  public var viewModel: BrandCardViewModel!
  
  var body: some View {
    HStack (spacing: 30){
      ForEach(0..<4) { index in
        BCContactsButtonView(index: index, viewModel: viewModel)
          .frame(width : viewModel.constraints.contactsSquare,
                 height: viewModel.constraints.contactsSquare)
      }
    }
    .padding(.vertical, viewModel.constraints.contactsViewPaddingV)
    .padding(.horizontal, viewModel.constraints.contactsViewPaddingH)
  }
}

struct BrandContactButtonsViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCContactsButtonsView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 99))
  }
}

