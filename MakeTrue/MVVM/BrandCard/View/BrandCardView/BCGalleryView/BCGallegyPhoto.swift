
import SwiftUI

struct BCGallegyPhoto: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    
    VStack {
      Image.set(.galleryPhotoBrandCard)
        .resizable()
        .aspectRatio(contentMode: .fill)
        .frame(width : viewModel.constraints.galleryPhotoViewWidth,
               height: viewModel.constraints.galleryPhotoViewHeight)
        .clipsToBounds(10)
        .shadow(corner: 10, radius: 5, color: .set(.shadowButtonContactBC))
    }
  }
}

struct BCGallegyPhotoPreviews: PreviewProvider {
  
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCGallegyPhoto(viewModel: viewModel)
      .previewLayout(.fixed(width: 66, height: 66))
  }
}
