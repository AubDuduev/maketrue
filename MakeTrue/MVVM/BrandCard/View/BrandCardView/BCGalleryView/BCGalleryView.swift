
import SwiftUI

struct BCGalleryView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
     // let customWidth = geo.size.width - viewModel.constraints.galleryPaddingH * 2
      VStack(spacing: 0) {
        //1 - Title
        HStack {
          Text("Фотографии")
            .font(Font.set(.LatoBlack, size: 12.5))
          Spacer()
          Image.set(.downArrowBrandCard)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.set(.arrowDownBarndCard))
            .frame(width: 16, height: 16)
        }
        .frame(height: 15)
        .padding(.top, viewModel.constraints.galleryPaddingT)
        .padding(.horizontal, viewModel.constraints.galleryPaddingH)
        
        //2 - Gallery preview
        Spacer().frame(width: 200, height: 24)
        //Main photo
        Image.set(.galleryPhotoBrandCard)
          .resizable()
          .aspectRatio(contentMode: .fill)
          .frame(width : viewModel.constraints.galleryMainPhotoWidth,
                 height: viewModel.constraints.galleryMainPhotoHeight, alignment: .center)
          .clipsToBounds(10)
          .shadow(corner: 10, radius: 7, color: .set(.shadowButtonContactBC))
          
        //3 - Other photos
        ScrollView(.horizontal, showsIndicators: false) {
          HStack(spacing: 0) {
            ForEach(0..<7) { i in
              BCGallegyPhoto(viewModel: viewModel)
                .frame(width : viewModel.constraints.galleryPhotoViewWidth,
                       height: viewModel.constraints.galleryPhotoViewHeight)
                .padding(5)
            }
          }
          .padding(.vertical, viewModel.constraints.galleryPhotoPaddingV)
          .padding(.leading, viewModel.constraints.galleryPhotoPaddingL)
        }
        Spacer(minLength: 20)
      }//end vstack
      
    }//end geometry reader
    .frame(width : viewModel.constraints.galleryViewWidth,
           height: viewModel.constraints.galleryViewHeight)
  }
}

struct BCGalleryViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCGalleryView(viewModel: viewModel)
//      //iPhone 5
//      .previewLayout(.fixed(width: 320.0, height: 568.0))
//      //iPhone 7
//      .previewLayout(.fixed(width: 375.0, height: 667.0))
//      //iPhone Plus
//      .previewLayout(.fixed(width: 414.0, height: 736.0))
//      //iPhone X
//      .previewLayout(.fixed(width: 375.0, height: 812.0))
      //iPhone X Max
      //.previewLayout(.fixed(width: 414.0, height: 408.25))
//      //iPhone 12
      .previewLayout(.fixed(width: 390.0, height: 384.5))
  }
}

