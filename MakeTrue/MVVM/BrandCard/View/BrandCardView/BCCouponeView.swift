
import SwiftUI

struct BCCouponeView: View {
  
  
public var viewModel: BrandCardViewModel!
  
  var body: some View {
    GeometryReader { geo in
     
      ZStack {
        RoundedRectangle(cornerRadius: 6)
          .foregroundColor(.white)
          .clipShape(RoundedRectangle(cornerRadius: 6))
          .shadow(color: .set(.shadowButtonContactBC), radius: 4)
          .padding(.vertical, viewModel.constraints.couponeViewPaddingV)
          .padding(.horizontal, viewModel.constraints.couponeViewPaddingH)
        
        HStack {
          Image.set(.couponesIconBarndCard)
            .resizable()
            .frame(width: 63, height: 40)
            .offset(x: 9, y: 0)
          Spacer()
          Text("Купоны")
            .font(Font.set(.latoRegular, size: 14))
          Spacer()
          Image.set(.fireCouponeBrandCard)
            .resizable()
            .frame(width : geo.size.height - (viewModel.constraints.couponeViewPaddingV * 2),
                   height: geo.size.height - (viewModel.constraints.couponeViewPaddingV * 2))
            .offset(x: 0, y: 0)
            .clipShape(RoundedRectangle(cornerRadius: 6))
        }
        .padding(.vertical, viewModel.constraints.couponeViewPaddingV)
        .padding(.horizontal, viewModel.constraints.couponeViewPaddingH)
      }
    }
  }
}

struct BCCouponeViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCCouponeView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 99))
  }
}
