//
//  BCMapView.swift
//  MakeTrue
//
//  Created by Senior Developer on 05.02.2021.
//
import MapKit
import SwiftUI

struct BCMapView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      let commonSize  = geo.size
      VStack(spacing: 15) {
        
        //MARK: 1 - Title
        HStack {
          Text("Адрес")
            .font(Font.set(.LatoBlack, size: 12.5))
          Spacer()
          Image.set(.downArrowBrandCard)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.set(.arrowDownBarndCard))
            .frame(width: 16, height: 16)
        }//end title
        .padding(.top, viewModel.constraints.mapPaddingT)
        .padding(.horizontal, viewModel.constraints.mapPaddingH)
        
        //MARK: 2 - Address
        HStack {
          Text("Москва, пл. Смоленская, д. 3")
            .frame(height: 15)
            .font(Font.set(.HelveticaNeue, size: 12.5))
          Spacer()
        }
        .frame(height: 15)
        .padding(.horizontal, viewModel.constraints.mapPaddingH)
        Spacer(minLength: 0)
        //3 - Map
        GDMapView()
          .frame(width : viewModel.constraints.gdMapViewWidth,
                 height: viewModel.constraints.gdMapViewHeight)
          .clipShape(RoundedRectangle(cornerRadius: 11))
          .shadow(color: .set(.shadowButtonContactBC), radius: 7)
        Spacer().frame(width: 100, height: viewModel.constraints.mapPaddingB)
      }//end common VStack
      .frame(width: commonSize.width, height: commonSize.height)
    }
  }
}
struct BCMapViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCMapView(viewModel: viewModel)
      //      //iPhone 5
      //      .previewLayout(.fixed(width: 320.0, height: 568.0))
      //      //iPhone 7
      //      .previewLayout(.fixed(width: 375.0, height: 667.0))
      //      //iPhone Plus
      //      .previewLayout(.fixed(width: 414.0, height: 736.0))
      //      //iPhone X
      //      .previewLayout(.fixed(width: 375.0, height: 812.0))
            //iPhone X Max
            .previewLayout(.fixed(width: 414.0, height: 408.25))
      //      //iPhone 12
      //      .previewLayout(.fixed(width: 390.0, height: 844.0))
  }
}
//let width  = Int(UIScreen.main.bounds.width)
//let height = Int(UIScreen.main.bounds.height)
//Text("width:\(width), height:\(height)")
//  .font(Font.set(.HelveticaNeue, size: 12.5))
