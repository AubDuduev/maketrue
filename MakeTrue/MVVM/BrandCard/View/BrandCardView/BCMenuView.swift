
import SwiftUI

struct BCMenuView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      
      ZStack {
        RoundedRectangle(cornerRadius: 6)
          .foregroundColor(.white)
          .clipShape(RoundedRectangle(cornerRadius: 6))
          .shadow(color: .set(.shadowButtonContactBC), radius: 4)
        HStack {
          Text("Меню")
            .font(Font.set(.HelveticaNeue, size: 16.5))
        }
      }
      .padding(.vertical, self.viewModel.constraints.menuViewPaddingV)
      .padding(.horizontal, self.viewModel.constraints.menuViewPaddingH)
    }
  }
}

struct BCMenuViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCMenuView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 99))
  }
}
