
import SwiftUI

struct BCTagsView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    
    GeometryReader { geo in
      let commonSize = geo.size
      //let customWidth = geo.size.width - viewModel.constraints.tagsPaddingH * 2
      VStack(spacing: 0) {
        //MARK: - Title
        HStack {
          Text("Теги")
            .font(Font.set(.LatoBlack, size: 12.5))
            .multilineTextAlignment(.leading)
          Spacer()
          Image.set(.downArrowBrandCard)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.set(.arrowDownBarndCard))
            .frame(width: 16, height: 16)
        }
        .frame(height: 15)
        .padding(.horizontal, viewModel.constraints.tagsPaddingH)
        //MARK: - Spacer 15
        Spacer().frame(width: 200, height: 15)
        //MARK: - Tags
        ScrollView(.horizontal, showsIndicators: false) {
          HStack {
            ForEach(0..<10) { i in
              BCTagView(nameTag: "Фьюжн")
                .frame(height: 25)
            }
          }
          .padding(.leading, viewModel.constraints.tagsPaddingH)
        }
        .frame(width: commonSize.width, height: 30)
        //MARK: - Spacer all bottom
        Spacer()
      }//end commob vstack
      .frame(width: commonSize.width, height: commonSize.height - viewModel.constraints.tagsPaddingT)
      .padding(.top, viewModel.constraints.tagsPaddingT)
    }
  }
}

struct BCTagsViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCTagsView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 99))
  }
}

