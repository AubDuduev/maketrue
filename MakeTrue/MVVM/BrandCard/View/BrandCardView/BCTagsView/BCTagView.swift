
import SwiftUI

struct BCTagView: View {
  
  public let nameTag: String!
  
  var body: some View {
    
    ZStack {
      RoundedRectangle(cornerRadius: 12.5)
        .strokeBorder(Color.gray, lineWidth: 1)
      Text(self.nameTag)
        .font(Font.set(.HelveticaNeue, size: 11))
        .padding(5)
    }
  }
}

struct BCTagViewPreviews: PreviewProvider {
  static var previews: some View {
    BCTagView(nameTag: "Фьюжн")
      .previewLayout(.fixed(width: 100, height: 22))
  }
}
