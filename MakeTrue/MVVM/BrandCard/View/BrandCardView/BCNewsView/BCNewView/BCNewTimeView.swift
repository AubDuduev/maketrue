
import SwiftUI

struct BCNewTimeView: View {
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      //MARK: - Time
      VStack {
        HStack {
          Spacer()
          Text("17.07.2021")
            .font(Font.set(.HelveticaNeue, size: 11))
        }
        .padding(.trailing, 10)
        Spacer().frame(width: commonSize.width, height: 5)
      }
    }
  }
}
