
import SwiftUI

struct BCNewDataView: View {
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      VStack(spacing: 0)  {
        //MARK: - Text name barnd
        VStack(spacing: 0)  {
          HStack {
            Text("PESHI")
              .font(Font.set(.latoRegular, size: 12.5))
              .padding(.leading, 16)
            Spacer()
            //Like dislike
            Image.set(.dislikeNewBrandCard)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .frame(width: 16, height: 15)
            Image.set(.likeNewBrandCard)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .frame(width: 16, height: 15)
              .padding(.trailing, 16)
              .padding(.leading, 26)
          }.frame(width: commonSize.width, height: 21.55)
          Divider().foregroundColor(.gray).frame(width: commonSize.width, height: 1)
        }
        .frame(width: commonSize.width, height: 23.55)
        //MARK: - Spacer
        Spacer().frame(width: commonSize.width, height: 5)
        //MARK: - Title and description
        VStack(spacing: 0) {
          //MARK: - Title
          HStack {
            Text("Будущее наступило")
              .font(Font.set(.latoRegular, size: 15))
              .multilineTextAlignment(.leading)
            Spacer()
          }
          .frame(width: commonSize.width - 32, height: 15, alignment: .leading)
          //MARK: - Description
          HStack {
            Text("Блюда с Beyond Meat в каждом ресторане WRF Чтобы быть супергероем и спасать планету, вовсе не нужно летать вдоль экватора в трико и плаще. Достаточно просто сделать осознанный …")
              .font(Font.set(.latoRegular, size: 10))
              .multilineTextAlignment(.leading)
          }
          .frame(width: commonSize.width - 32, height: 67, alignment: .leading)
        }
        .frame(width: commonSize.width - 32, height: 82, alignment: .leading)
        //MARK: - Spacer
        Spacer().frame(width: commonSize.width, height: 5)
        //MARK: - Button see site
        HStack {
          Button(action: {}, label: {
            Text("Посмотреть на сайте")
              .frame(width: commonSize.width - 36, height: 27.17)
              .font(Font.set(.latoRegular, size: 12.5))
              .foregroundColor(Color.set(.blueProject))
              .background(RoundedRectangle(cornerRadius: 7).stroke(Color.set(.blueProject)))
          })
        }
        Spacer().frame(width: commonSize.width, height: 20)
      }
      .frame(width: commonSize.width, height: commonSize.height)
    }
  }
}

struct BCNewDataViewPreviews: PreviewProvider {
  static var previews: some View {
    BCNewDataView()
      .previewDisplayName(/*@START_MENU_TOKEN@*/"Preview"/*@END_MENU_TOKEN@*/)
      .previewDevice(/*@START_MENU_TOKEN@*/"iPhone 11"/*@END_MENU_TOKEN@*/)
      .previewLayout(.fixed(width: 309.78, height: 171.2))
     
  }
}
