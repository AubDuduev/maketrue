
import SwiftUI

struct BCNewView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      //MARK: - common VStack
      VStack(spacing: 0) {
        BCNewTimeView()
          .frame(width: commonSize.width,
                 height: viewModel.constraints.newTimeViewHeight)
        VStack(spacing: 0) {
          BCNewImageView()
            .frame(width: commonSize.width,
                   height: viewModel.constraints.newImageViewHeight)
          BCNewDataView()
            .frame(width: commonSize.width,
                   height: viewModel.constraints.newDataViewHeight)
        }
        .clipsToBounds(10)
        .shadow(corner: 10, radius: 5, color: .set(.shadowButtonContactBC))
      }//end common VStack
      .frame(width : viewModel.constraints.newViewWidth,
             height: viewModel.constraints.newViewHeight)
    }
  }
}
struct BCNewView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCNewView(viewModel: viewModel)
      //iPhone 5
     // .previewLayout(.fixed(width: 320.0, height: 568.0))
      //iPhone 7
     // .previewLayout(.fixed(width: 375.0, height: 667.0))
      //iPhone Plus
     // .previewLayout(.fixed(width: 414.0, height: 736.0))
      //iPhone X
    //  .previewLayout(.fixed(width: 375.0, height: 812.0))
      //iPhone X Max
      .previewLayout(.fixed(width: 342, height: 390))
      //iPhone 12
     // .previewLayout(.fixed(width: 390.0, height: 844.0))
  }
}







