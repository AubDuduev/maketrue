
import SwiftUI

struct BCNewImageView: View {
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      //MARK: - Image blude
      Image.set(.imageBludeNewBrandCard)
        .resizable()
        .frame(width: commonSize.width, height: commonSize.height)
        .aspectRatio(contentMode: .fill)
        .clipped()
      
    }
  }
}
