
import SwiftUI

struct BCNewsView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      let commonSize  = geo.size
      //let customWidth = geo.size.width - viewModel.constraints.newsPaddingH * 2
      VStack(spacing: 0) {
        //MARK: 1 - Title
        HStack {
          Text("Новости")
            .font(Font.set(.LatoBlack, size: 12.5))
          Spacer()
          Image.set(.downArrowBrandCard)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.set(.arrowDownBarndCard))
            .frame(width: 16, height: 16)
        }//end title
        .padding(.top, viewModel.constraints.newsPaddingT)
        .padding(.horizontal, viewModel.constraints.newsPaddingH)
        
        //MARK: 2 - Spacer 15
        Spacer().frame(width: 200, height: 15)
        //MARK: 3 - BCNewView
        ScrollView(.horizontal, showsIndicators: false) {
          HStack(spacing: 21) {
            ForEach(0..<4) { i in
              BCNewView(viewModel: viewModel)
                .frame(width : viewModel.constraints.newViewWidth,
                       height: viewModel.constraints.newViewHeight)
            }
          }
          .padding(.leading, viewModel.constraints.newScrollPaddingL)
          .padding(.bottom, viewModel.constraints.newScrollPaddingB)
        }
        Spacer()
      }
      .frame(width: commonSize.width, height: commonSize.height)
    }
  }
}

struct BCNewsView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCNewsView(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 442))
  }
}
