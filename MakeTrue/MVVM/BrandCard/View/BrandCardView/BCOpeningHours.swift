
import SwiftUI

struct BCOpeningHours: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      let customWidth = geo.size.width - viewModel.constraints.openingPaddingH * 2
      VStack(spacing: 0) {
        //Title
        HStack {
          Text("Часы работы")
            .font(Font.set(.LatoBlack, size: 12.5))
            .multilineTextAlignment(.leading)
          Spacer()
          Image.set(.downArrowBrandCard)
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.set(.arrowDownBarndCard))
            .frame(width: 16, height: 16)
        }
        Spacer().frame(width: customWidth, height: 15)
        //Hours
        HStack {
          Text("Пн - 10:00 - 23:00")
            .multilineTextAlignment(.leading)
            .frame(height: 15)
            .font(Font.set(.HelveticaNeue, size: 12.5))
          Spacer()
        }
        Spacer().frame(width: customWidth, height: 41)
      }
      .padding(.horizontal, viewModel.constraints.openingPaddingH)
      .padding(.top, viewModel.constraints.openingPaddingT)
    }
  }
}

struct BCOpeningHoursPreviews: PreviewProvider {
  static var previews: some View {
    let viewModel = BrandCardViewModel()
    BCOpeningHours(viewModel: viewModel)
      .previewLayout(.fixed(width: 375, height: 99))
  }
}
