
import UIKit
import SwiftUI
import Combine

struct BrandCardView: View {
  
  public var viewModel = BrandCardViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: BrandCardViewData
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      ZStack {
        ScrollView(showsIndicators: false) {
          VStack(spacing: 0) {
            //MARK: - BCHeaderView
            Group {
              BCHeaderView(viewModel: self.viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.headerViewHeight)
              Divider()
            }
            //MARK: - BrandContactButtonsView
            Group {
              BCCouponeView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.couponeViewHeight)
              Divider()
            }
            //MARK: - BrandContactButtonsView
            Group {
              BCContactsButtonsView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.contactsButtonsViewHeight)
              Divider()
            }
            //MARK: - BCDescriptionView
            Group {
              BCDescriptionView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.descriptionViewHeight)
              Divider()
            }
            //MARK: - BCMenuView
            Group {
              BCMenuView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.menuViewHeight)
              Divider()
            }
            //MARK: - BCOpeningHours
            Group {
              BCOpeningHours(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.openingHoursHeight)
              Divider()
            }
            //MARK: - BCTagsView
            Group {
              BCTagsView(viewModel: viewModel)
                .frame(width : commonSize.width,
                       height: viewModel.constraints.tagsViewHeight)
              Divider()
            }
            //MARK: - BCNewsView
            Group {
              BCNewsView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.newsViewHeight)
              Divider()
            }
            //MARK: - BCGalleryView
            Group {
              BCGalleryView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.galleryViewHeight)
              Divider()
            }
            //MARK: - BCMapView
            Group {
              BCMapView(viewModel: viewModel)
                .frame(width: commonSize.width,
                       height: viewModel.constraints.mapViewHeight)
              Divider()
            }
          }//end common vstack
          .edgesIgnoringSafeArea(.all)
        }//end scrollView
        .edgesIgnoringSafeArea(.all)
      }//end common zstack
      ActionRestarauntView(viewModel: viewModel)
    }//end common geometry reader
    .edgesIgnoringSafeArea(.all)
    .navigationBarHidden(true)
    
    //Life cycle
    .onAppear(){
      
    }
  }
}

struct BrandCardPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = BrandCardViewData()
    BrandCardView(viewData: viewData)
      //iPhone 5
      //.previewLayout(.fixed(width: 320.0, height: 568.0))
      //iPhone 7
      //.previewLayout(.fixed(width: 375.0, height: 667.0))
      //iPhone Plus
      //.previewLayout(.fixed(width: 414.0, height: 736.0))
      //iPhone X
      .previewLayout(.fixed(width: 375.0, height: 812.0))
      //iPhone X Max
      //.previewLayout(.fixed(width: 414.0, height: 896.0))
      //iPhone 12
      //.previewLayout(.fixed(width: 390.0, height: 844.0))
  }
}
