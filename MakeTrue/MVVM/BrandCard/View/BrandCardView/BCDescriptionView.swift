
import SwiftUI

struct BCDescriptionView: View {
  
  public var viewModel: BrandCardViewModel
  
  var body: some View {
    GeometryReader { geo in
      
      let commonWidth = geo.size.width  - self.viewModel.constraints.descriptionTextPaddingH * 2
      let commonHight = geo.size.height - self.viewModel.constraints.descriptionTextPaddingV * 2
      VStack {
        Text("Панорама города на 360 градусов — главная особенность ресторана. Отсюда видно Новый Арбат, купола Храма Христа Спасителя, гостиницу «УкПанорама города на 360 градусов — главная особенность ресторана. Отсюда видно Новый Арбат, купола Храма Христа Спасителя, гостиницу «Ук")
          .font(Font.set(.latoRegular, size: 14))
          .multilineTextAlignment(.leading)
          .frame(width: commonWidth, height: commonHight)
      }
      
      .padding(.vertical, self.viewModel.constraints.descriptionTextPaddingV)
      .padding(.horizontal, self.viewModel.constraints.descriptionTextPaddingH)
    }
  }
}

struct BCDescriptionView_Previews: PreviewProvider {
    static var previews: some View {
      let viewModel = BrandCardViewModel()
      BCDescriptionView(viewModel: viewModel)
      .previewDevice("iPhone 11")
      .previewDisplayName("Preview")
      .previewLayout(.fixed(width: 375, height: 99))
    }
}
