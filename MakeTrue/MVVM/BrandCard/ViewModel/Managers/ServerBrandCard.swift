import UIKit
import Protocols

class ServerBrandCard: VMManager {
  
  //MARK: - Public variable
  public var VM: BrandCardViewModel!
  
  
}
//MARK: - Initial
extension ServerBrandCard {
  
  //MARK: - Inition
  convenience init(viewModel: BrandCardViewModel) {
    self.init()
    self.VM = viewModel
  }
}


