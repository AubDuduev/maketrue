import Foundation
import Protocols

class LogicBrandCard: VMManager {
  
  //MARK: - Public variable
  public var VM: BrandCardViewModel!
  
  
}
//MARK: - Initial
extension LogicBrandCard {
  
  //MARK: - Inition
  convenience init(viewModel: BrandCardViewModel) {
    self.init()
    self.VM = viewModel
  }
}
