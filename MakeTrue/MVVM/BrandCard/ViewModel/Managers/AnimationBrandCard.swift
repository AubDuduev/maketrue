import UIKit
import SwiftUI
import Protocols

class AnimationBrandCard: VMManager {
  
  //MARK: - Public variable
  public var VM: BrandCardViewModel!
  
  public func actionsAnimation(view: ActionRestarauntView){
    
    if view.isPresentBackgraund {
      view.opacity = 0
    } else {
      withAnimation(.easeInOut(duration: 0.3)) {
        view.opacity = 1
      }
    }
    view.isPresentBackgraund.toggle()
  }
  public func subscribeAnimation(view: BCHeaderView){
    if view.isSubscribe {
      view.lottieView.lottieSetup.playProgress(fromProgress: 0.0, toProgress: 0.5)
    } else {
      view.lottieView.lottieSetup.playProgress(fromProgress: 0.5, toProgress: 1.0)
    }
    view.isSubscribe.toggle()
  }
}
//MARK: - Initial
extension AnimationBrandCard {
  
  //MARK: - Inition
  convenience init(viewModel: BrandCardViewModel) {
    self.init()
    self.VM = viewModel
  }
}

