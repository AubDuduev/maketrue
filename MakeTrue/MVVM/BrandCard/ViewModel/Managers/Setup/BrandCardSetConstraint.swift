
import UIKit
import Foundation

class BrandCardSetConstraint {
  
  
  public func setConstraint() -> BrandCardConstraintoble {
    print(GDScreenDevice.get().rawValue, "current device")
    
    switch GDScreenDevice.get() {
      
      //1 - iPhone 5
      case .iPhone5:
        return iPhoneXBrandCardConstraints()
      //2 - iPhone 7
      case .iPhone7:
        return iPhoneXBrandCardConstraints()
      //3 - iPhone Plus
      case .iPhonePlus:
        return iPhonePlusBrandCardConstraints()
      //4 - iPhone X
      case .iPhoneX:
        return iPhoneXBrandCardConstraints()
      //5 - iPhone 12
      case .iPhone12:
        return iPhone12BrandCardConstraints()
      //6 - iPhone MAX
      case .iPhoneMax:
        return iPhoneMaxBrandCardConstraints()
      //7 - iPhone 12 MAX
      case .iPhone12Max:
        return iPhone12MaxBrandCardConstraints()
    }
  }
}



