
import UIKit
import Protocols
import SwiftUI
//import SnapKit


class SetupBrandCard: VMManager {
  
  //MARK: - Public variable
  public var VM: BrandCardViewModel!
  
  public func hostingVC(){
//    let view     = BrandCardView(viewData: self.VM.viewData)
//    let hostingVC = UIHostingController(rootView: view)
//    self.VM.VC.view.addSubview(hostingVC.view)
//    self.VM.VC.addChild(hostingVC)
//    hostingVC.view.snp.makeConstraints { (hostingVCView) in
//      hostingVCView.edges.equalTo(self.VM.VC.view)
//    }
  }
  public func cancellableViewData(){
//    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
//      print(name)
//    })
  }
  public func choiceDevice() -> BrandCardConstraintoble {
    let setConstraint = BrandCardSetConstraint()
    return setConstraint.setConstraint()
  }
  public func lottieView(view: BCHeaderView){
    view.lottieView.setupLottie(lottieFile: .subscribeBrandCard)
    self.VM.managers.animation.subscribeAnimation(view: view)
  }
}
//MARK: - Initial
extension SetupBrandCard {
  
  //MARK: - Inition
  convenience init(viewModel: BrandCardViewModel) {
    self.init()
    self.VM = viewModel
  }
}


