
import UIKit
import Protocols

class PresentBrandCard: VMManager {
  
  //MARK: - Public variable
  public var VM: BrandCardViewModel!
  
  
}
//MARK: - Initial
extension PresentBrandCard {
  
  //MARK: - Inition
  convenience init(viewModel: BrandCardViewModel) {
    self.init()
    self.VM = viewModel
  }
}

