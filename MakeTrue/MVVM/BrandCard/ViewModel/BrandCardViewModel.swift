
import Foundation
import Protocols
import SwiftUI
import Combine

class BrandCardViewModel: VMManagers {
	
	public var brandCardModel: BrandCardModel = .loading {
		didSet{
			self.commonLogicBrandCard()
		}
	}
  
  //MARK: - Public variable
  public var managers    : BrandCardManagers!
  public var VC          : BrandCardViewController!
  public var view        : BrandCardView!
  public var cancellable : AnyCancellable!
  public var constraints : BrandCardConstraintoble {
    get {
      return self.managers.setup.choiceDevice()
    }
  }
  
  public func commonLogicBrandCard(){
    
    switch self.brandCardModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = BrandCardManagers(viewModel: self)
  }
}
//MARK: - Initial
extension BrandCardViewModel {
  
  convenience init(viewController: BrandCardViewController) {
    self.init()
    self.VC       = viewController
    self.managers = BrandCardManagers(viewModel: self)
  }
}
