
import UIKit
import SwiftUI
import Combine

struct OrdersView: View {
  
  @State private var viewModel  = OrdersViewModel()
  @State private var lottieView = GDLottieView()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: OrdersViewData
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack {
        Image.set(.noOrders)
          .frame(width    : commonSize.width,
                 height   : commonSize.height,
                 alignment: .center)
      }
    }
    .onAppear(){
      self.lottieView.setupLottie(lottieFile: .logoLoad, loopMode: .autoReverse)
      self.lottieView.lottieSetup.play()
    }
  }
}

struct OrdersPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = OrdersViewData()
    OrdersView(viewData: viewData)
  }
}
