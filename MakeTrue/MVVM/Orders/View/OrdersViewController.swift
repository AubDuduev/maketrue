import UIKit

class OrdersViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: OrdersViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = OrdersViewModel(viewController: self)
  }
}
