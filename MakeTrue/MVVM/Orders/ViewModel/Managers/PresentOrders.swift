
import UIKit
import Protocols

class PresentOrders: VMManager {
  
  //MARK: - Public variable
  public var VM: OrdersViewModel!
  
  
}
//MARK: - Initial
extension PresentOrders {
  
  //MARK: - Inition
  convenience init(viewModel: OrdersViewModel) {
    self.init()
    self.VM = viewModel
  }
}

