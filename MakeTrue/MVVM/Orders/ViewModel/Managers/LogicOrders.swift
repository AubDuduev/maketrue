import Foundation
import Protocols

class LogicOrders: VMManager {
  
  //MARK: - Public variable
  public var VM: OrdersViewModel!
  
  
}
//MARK: - Initial
extension LogicOrders {
  
  //MARK: - Inition
  convenience init(viewModel: OrdersViewModel) {
    self.init()
    self.VM = viewModel
  }
}
