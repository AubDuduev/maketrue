
import Foundation
import Protocols
import SwiftUI
import Combine

class OrdersViewModel: VMManagers {
	
	public var OrdersModel: OrdersModel = .loading {
		didSet{
			self.commonLogicOrders()
		}
	}
  
  //MARK: - Public variable
  public var managers    : OrdersManagers!
  public var VC          : OrdersViewController!
  public var viewData    = OrdersViewData()
  public var view        : OrdersView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicOrders(){
    
    switch self.OrdersModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension OrdersViewModel {
  
  convenience init(viewController: OrdersViewController) {
    self.init()
    self.VC       = viewController
    self.managers = OrdersManagers(viewModel: self)
  }
}
