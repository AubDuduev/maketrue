
import Foundation
import Protocols

class OrdersManagers: VMManagers {
  
  let setup    : SetupOrders!
  let server   : ServerOrders!
  let present  : PresentOrders!
  let logic    : LogicOrders!
  let animation: AnimationOrders!
  let router   : RouterOrders!
  
  init(viewModel: OrdersViewModel) {
    
    self.setup     = SetupOrders(viewModel: viewModel)
    self.server    = ServerOrders(viewModel: viewModel)
    self.present   = PresentOrders(viewModel: viewModel)
    self.logic     = LogicOrders(viewModel: viewModel)
    self.animation = AnimationOrders(viewModel: viewModel)
    self.router    = RouterOrders(viewModel: viewModel)
  }
}

