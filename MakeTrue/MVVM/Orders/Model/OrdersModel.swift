
import UIKit

enum OrdersModel {
	
	case loading
	case getData
	case errorHandler(OrdersViewData?)
	case presentData(OrdersViewData)
	
	
}

