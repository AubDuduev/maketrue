
import Foundation
import Protocols
import SwiftUI
import Combine

class CouponsViewModel: VMManagers {
	
	public var CouponsModel: CouponsModel = .loading {
		didSet{
			self.commonLogicCoupons()
		}
	}
  
  //MARK: - Public variable
  public var managers    : CouponsManagers!
  public var VC          : CouponsViewController!
  public var viewData    = CouponsViewData()
  public var view        : CouponsView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicCoupons(){
    
    switch self.CouponsModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension CouponsViewModel {
  
  convenience init(viewController: CouponsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = CouponsManagers(viewModel: self)
  }
}
