
import UIKit
import Protocols

class PresentCoupons: VMManager {
  
  //MARK: - Public variable
  public var VM: CouponsViewModel!
  
  
}
//MARK: - Initial
extension PresentCoupons {
  
  //MARK: - Inition
  convenience init(viewModel: CouponsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

