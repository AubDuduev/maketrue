import Foundation
import Protocols

class LogicCoupons: VMManager {
  
  //MARK: - Public variable
  public var VM: CouponsViewModel!
  
  
}
//MARK: - Initial
extension LogicCoupons {
  
  //MARK: - Inition
  convenience init(viewModel: CouponsViewModel) {
    self.init()
    self.VM = viewModel
  }
}
