
import Foundation
import Protocols

class CouponsManagers: VMManagers {
  
  let setup    : SetupCoupons!
  let server   : ServerCoupons!
  let present  : PresentCoupons!
  let logic    : LogicCoupons!
  let animation: AnimationCoupons!
  let router   : RouterCoupons!
  
  init(viewModel: CouponsViewModel) {
    
    self.setup     = SetupCoupons(viewModel: viewModel)
    self.server    = ServerCoupons(viewModel: viewModel)
    self.present   = PresentCoupons(viewModel: viewModel)
    self.logic     = LogicCoupons(viewModel: viewModel)
    self.animation = AnimationCoupons(viewModel: viewModel)
    self.router    = RouterCoupons(viewModel: viewModel)
  }
}

