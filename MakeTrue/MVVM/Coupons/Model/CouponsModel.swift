
import UIKit

enum CouponsModel {
	
	case loading
	case getData
	case errorHandler(CouponsViewData?)
	case presentData(CouponsViewData)
	
	
}

