import UIKit

class CouponsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: CouponsViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = CouponsViewModel(viewController: self)
  }
}
