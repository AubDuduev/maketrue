
import UIKit
import SwiftUI
import Combine

struct CouponsView: View {
  
  @State private var viewModel = CouponsViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: CouponsViewData
  
  var body: some View {
    
    VStack {
      
    }
    .onAppear(){
      self.viewModel.cancellable = self.viewData.$name.sink(receiveValue: { (name) in
       
      })
    }
  }
}

struct CouponsPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = CouponsViewData()
    CouponsView(viewData: viewData)
  }
}
