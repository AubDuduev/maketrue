import UIKit

class MainTabBarViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: MainTabBarViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = MainTabBarViewModel(viewController: self)
  }
}
