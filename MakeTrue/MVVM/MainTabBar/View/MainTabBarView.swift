
import UIKit
import SwiftUI
import Combine

struct MainTabBarView: View {
  
  @State private var viewModel = MainTabBarViewModel()
  @State private var selection: Int = 0
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: MainTabBarViewData
  
  var body: some View {
    NavigationView {
      TabView(selection: $selection){
        
        let mainViewData = MainViewData()
        MainView(viewData: mainViewData)
          .tabItem {
            ItemTabBar(title: .main, image: .mainIcon)
          }.tag(0)
        
        let newsViewData = NewsViewData()
        NewsView(viewData: newsViewData)
          .tabItem {
            ItemTabBar(title: .news, image: .newsIcon)
          }.tag(1)
        
        let couponsViewData = CouponsViewData()
        CouponsView(viewData: couponsViewData)
          .tabItem {
            ItemTabBar(title: .coupone, image: .couponeIcon)
          }.tag(2)
        
        let ordersViewData = OrdersViewData()
        OrdersView(viewData: ordersViewData)
          .tabItem {
            ItemTabBar(title: .orders, image: .orderIcon)
          }.tag(3)
        
        let profileViewData = ProfileViewData()
        ProfileView(viewData: profileViewData)
          .tabItem {
            ItemTabBar(title: .profile, image: .profileIcon)
          }.tag(4)
      }
      .accentColor(.black)
      .navigationBarHidden(true)
    }
    .onAppear(){
      
    }
  }
}

struct MainTabBarPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = MainTabBarViewData()
    MainTabBarView(viewData: viewData)
      .previewDevice(/*@START_MENU_TOKEN@*/"iPhone 11"/*@END_MENU_TOKEN@*/)
      .previewDisplayName("iPhone 11 Pro max real")
      .previewLayout(.fixed(width: 375.0, height: 812.0))
  }
}

struct ItemTabBar: View {
  
  public let title: TabBarNames!
  public let image: Image.ImageNames!
  
  var body: some View {
    
    VStack {
      Image.set(self.image)
        .resizable()
        .renderingMode(.template)
        .aspectRatio(contentMode: .fit)
        .frame(width: 20, height: 20)
        .foregroundColor(.black)
      Text(self.title.rawValue)
    }
  }
}

enum TabBarNames: String {
  
  case main    = "Главная"
  case news    = "Лента"
  case coupone = "Купоны"
  case orders  = "Мои заказы"
  case profile = "Я"
}
