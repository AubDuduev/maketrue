
import Foundation
import Protocols
import SwiftUI
import Combine

class MainTabBarViewModel: VMManagers {
	
	public var mainTabBarModel: MainTabBarModel = .loading {
		didSet{
			self.commonLogicMainTabBar()
		}
	}
  
  //MARK: - Public variable
  public var managers    : MainTabBarManagers!
  public var VC          : MainTabBarViewController!
  public var view        : MainTabBarView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicMainTabBar(){
    
    switch self.mainTabBarModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = MainTabBarManagers(viewModel: self)
  }
}
//MARK: - Initial
extension MainTabBarViewModel {
  
  convenience init(viewController: MainTabBarViewController) {
    self.init()
    self.VC       = viewController
    self.managers = MainTabBarManagers(viewModel: self)
  }
}
