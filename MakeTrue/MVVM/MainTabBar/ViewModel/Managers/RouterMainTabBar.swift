
import UIKit
import Protocols

class RouterMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension RouterMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}



