import UIKit
import Protocols

class ServerMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension ServerMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}


