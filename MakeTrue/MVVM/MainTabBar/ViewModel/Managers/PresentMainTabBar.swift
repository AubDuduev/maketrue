
import UIKit
import Protocols

class PresentMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension PresentMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}

