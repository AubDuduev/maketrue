import UIKit
import Protocols

class AnimationMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModel!
  
  
}
//MARK: - Initial
extension AnimationMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}

