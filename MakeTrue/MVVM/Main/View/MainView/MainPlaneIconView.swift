
import SwiftUI

struct MainPlaneIconView: View {
  var body: some View {
    GeometryReader { geo in
     // let commonSize = geo.size
      ZStack {
        Image.set(.mainPlaneIcon)
          .resizable()
          .aspectRatio(contentMode: .fit)
      }
      .shadow(color: .black, radius: 5)
    }
  }
}

struct MainPlaneIconView_Previews: PreviewProvider {
  static var previews: some View {
    MainPlaneIconView()
      .previewDevice(/*@START_MENU_TOKEN@*/"iPhone 11"/*@END_MENU_TOKEN@*/)
      .previewLayout(.fixed(width: 219, height: 22))
  }
}
