
import SwiftUI

//MARK: - MainHeaderImageView
struct MainHeaderImageView: View {
  
  @EnvironmentObject private var viewModel: MainViewModel
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      ZStack {
        VStack {
          Image.set(.moscowHeader)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .clipped(antialiased: false)
        }
        .modifier(OffsetStretch(geo: geo))
        .modifier(FrameStretch(geo: geo))
        //Black view
        Rectangle()
          .foregroundColor(.set(.balckOpacity))
          .frame(width: commonSize.width, height: commonSize.height)
      }.frame(width: commonSize.width, height: commonSize.height)
    }
  }
}
struct MainHeaderViewPreviews: PreviewProvider {
  static var previews: some View {
    MainHeaderImageView()
      //Iphone 11 Pro max
      .previewLayout(.fixed(width: 375, height: 292))
      //Iphone 11 Max
      .previewLayout(.fixed(width: 414, height: 323))
  }
}

