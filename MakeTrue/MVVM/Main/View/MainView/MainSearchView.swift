
import SwiftUI

//MARK: - Search view
struct MainSearchView: View {
  
  @State private var searchText: String = ""
  
  var body: some View {
    GeometryReader { geo in
      let commonSize = geo.size
      ZStack {
        //Fon
        RoundedRectangle(cornerRadius: 6)
          .foregroundColor(Color.white)
          .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6)), radius: 5)
        HStack(spacing: 20) {
          //Loupe
          Image.set(.loupe)
            .resizable()
            .renderingMode(.template)
            .frame(width: 20, height: 20, alignment: .center)
          //Text field
          TextField("Любимые бренды, рестораны, отели…", text: $searchText)
            //.multilineTextAlignment(.leading)
            .font(Font.set(.HelveticaNeue, size: 14))
            .frame(height: 20, alignment: .leading)
        }
        .padding(.horizontal, 16)
      }
      .frame(width: commonSize.width, height: commonSize.height, alignment: .center)
    }
  }
}
struct MainSearchViewPreviews: PreviewProvider {
  static var previews: some View {
    MainSearchView()
      .environment(\.sizeCategory, .extraLarge)
      .previewLayout(.fixed(width: 382, height: 62))

  }
}

