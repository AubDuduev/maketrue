
import UIKit
import SwiftUI
import Combine

struct MainView: View {
  
  @ObservedObject private var viewModel = MainViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: MainViewData
  
  var body: some View {
    
    GeometryReader { commonGeo in
      let commonSize = commonGeo.size
      ZStack {
        Group {
          //MARK: - Common VStack ScrollView
          VStack(spacing: 0) {
            //MARK: - Header image
            Group {
              MainHeaderImageView()
                .environmentObject(self.viewModel)
            }
            .frame(width : commonSize.width,
                   height: viewModel.constraints.headerImageHeight)
            //MARK: - Categiries
            Group {
              ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 12) {
                  ForEach(0..<3, id: \.self){ indexRow in
                    MainIconCategoryView(indexRow: indexRow)
                      .environmentObject(self.viewModel)
                      .frame(width: viewModel.constraints.categiriesIconSquare,
                             height: viewModel.constraints.categiriesIconSquare)
                  }.foregroundColor(.blue)
                }
                .padding(.leading, viewModel.constraints.categiriesHStackLPadding)
              }
              .padding(.top, viewModel.constraints.categiriesHStackPaddingT)
              .padding(.bottom, viewModel.constraints.categiriesHStackPaddingB)
              .frame(width: commonSize.width, height: viewModel.constraints.categiriesHeight)
            }
            //MARK: - Texts vstack
            Group {
              MainCouponeTtitleView()
                .environmentObject(self.viewModel)
                .frame(width: commonSize.width, height: viewModel.constraints.couponeTitleHeight)
            }
            //MARK: - Collection coupone
            Group {
              ScrollView(.horizontal, showsIndicators: false){
                HStack(spacing: 0) {
                  ForEach(0..<3, id: \.self) { i in
                    MainCouponeCellView()
                      .environmentObject(self.viewModel)
                      .frame(width : viewModel.constraints.couponeWidth,
                             height: viewModel.constraints.couponeHeight)
                      .padding(.leading, 16)
                      .padding(.vertical, viewModel.constraints.couponeScrollVPadding)
                  }
                }
              }
            }
            Spacer(minLength: viewModel.constraints.commonViewPaddingB)
          }
          .frame(width: commonSize.width, height: commonSize.height)
          .edgesIgnoringSafeArea(.all)
        }
        
        //MARK: - Added MainCityDateView MainSearchView
        Group {
          let positionX = commonSize.width / 2
          let positionY = commonGeo.frame(in: .local).minY + viewModel.constraints.dateSearchViewsPositionY
          VStack(spacing: 0) {
            //Header city and date
            MainPlaneIconView()
              .frame(width: viewModel.constraints.dateViewWidth, height: 22, alignment: .center)
            //Spacer 5
            Spacer().frame(width: 200, height: 9)
            //Header city and date
            MainCityDateView()
              .frame(width : viewModel.constraints.dateViewWidth,
                     height: viewModel.constraints.dateViewHeight)
            //Spacer 5 spacingDateSearchHeight
            Spacer().frame(width: 200, height: viewModel.constraints.spacingDateSearchHeight)
            //Search text
            MainSearchView()
              .frame(height: viewModel.constraints.searchViewHeight)
          }
          .padding()
          .position(x: positionX, y: positionY)
        }
      }
    }
    .edgesIgnoringSafeArea(.all)
    .navigationBarHidden(true)
    //LifeCycle
    .onAppear(){
      
    }
  }
}

struct MainPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = MainViewData()
    MainView(viewData: viewData)
//      //iPhone 5
//      .previewLayout(.fixed(width: 320.0, height: 568.0))
//      //iPhone 7
//      .previewLayout(.fixed(width: 375.0, height: 667.0))
      //iPhone Plus
 //     .previewLayout(.fixed(width: 414.0, height: 736.0))
//      //iPhone X
 //     .previewLayout(.fixed(width: 375.0, height: 812.0))
//      //iPhone X Max
//      .previewLayout(.fixed(width: 414.0, height: 896.0))
//      //iPhone 12
      .previewLayout(.fixed(width: 390.0, height: 844.0))
  }
}





