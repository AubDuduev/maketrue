
import SwiftUI

//MARK: - MainCityDateView
struct MainCityDateView: View {
  
  var body: some View {
    
    ZStack {
      RoundedRectangle(cornerRadius: 8)
        .foregroundColor(Color.white)
        .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6)), radius: 3)
      HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5) {
        //Icon pin
        Image.set(.pin)
          .resizable()
          .renderingMode(.template)
          .frame(width: 16, height: 16, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        //Ttext city, date
        Text("Mосква, 25.12 - 28.12")
          .multilineTextAlignment(.leading)
          .font(Font.set(.HelveticaNeue, size: 12.68))
        //Arow up
        Image.set(.downArrow)
          .resizable()
          .renderingMode(.template)
          .frame(width: 12, height: 12, alignment: .center)
      }.frame(width: 184, height: 16, alignment: .center)
    }
    
  }
}

struct MainCityDateViewPreviews: PreviewProvider {
  static var previews: some View {
    MainCityDateView()
      .environment(\.sizeCategory, .extraLarge)
      .previewLayout(.fixed(width: 203.0, height: 35))

  }
}
