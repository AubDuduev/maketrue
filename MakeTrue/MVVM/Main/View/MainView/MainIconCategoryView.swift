
import SwiftUI
import Combine

//MARK: - MainIconCategoryView
struct MainIconCategoryView: View {
  
  @EnvironmentObject private var viewModel: MainViewModel
  
  public let indexRow : Int!
  
  @State public var lottieView = GDLottieView()
  
  var body: some View {
    GeometryReader { geo in
      let size = geo.size
      ZStack {
        Image.set(.sotie)
          .resizable()
          .renderingMode(.template)
          .foregroundColor(.set(.blueProject))
        VStack(spacing: 6) {
          self.lottieView
            .frame(width: 45, height: 45)
          Text(NameCategories.allCases[self.indexRow].rawValue)
            .font(Font.set(.HelveticaNeue, size: 12))
            .foregroundColor(.black)
        }.onAppear {
          self.viewModel.managers.setup.lottieSetup(view: self)
        }
        .offset(x: 0, y: -10)
        .frame(width: size.width - 25,
               height: size.height - 25,
               alignment: .center)
      }.onTapGesture {
        self.viewModel.managers.logic.play(view: self)
      }
    }
  }
}
struct MainCategoryViewPreviews: PreviewProvider {
  static var previews: some View {
    MainIconCategoryView(indexRow: 0)
      .environment(\.sizeCategory, .extraLarge)
      .previewLayout(.fixed(width: 110, height: 120))

  }
}

enum NameCategories: String, CaseIterable {
  
  case restourant = "Рестораны"
  case hotel      = "Отели"
  case shoping    = "Магазины"
}

enum LottieCategories: String, CaseIterable  {
  case mainRestoraunt
  case mainHotel
  case mainMagaz
}
