
import SwiftUI

//MARK: - CouponeCellView
struct MainCouponeCellView: View {
  
  @State private var isPushBarndCard = false
  
  @EnvironmentObject private var viewModel: MainViewModel
  
  private var brandCardViewData = BrandCardViewData()
  
  var body: some View {
    GeometryReader { geo in
      ZStack {
        
        VStack {
          ZStack {
            Image.set(.imageCouponeCell)
              .resizable()
              .aspectRatio(contentMode: .fill)
              .frame(width: geo.size.width, height: viewModel.constraints.couponeCellImageHight, alignment: .center)
              .clipShape(Rectangle())
            NavigationLink(
              destination: BrandCardView(viewData: self.brandCardViewData),
              isActive: self.$isPushBarndCard,
              label: {
                Circle().foregroundColor(.clear)
              })
          }
          Text("Скидка 15% на основное меню")
            .multilineTextAlignment(.leading)
            .lineLimit(2)
            .font(Font.set(.HelveticaNeue, size: 11))
            .truncationMode(.head)
            .frame(width: geo.size.width - 32, height: 30, alignment: .leading)
          Spacer(minLength: 8.5)
          //.lineLimit(2)
          Button(action: {}, label: {
            Text("Получить QR code")
              .font(Font.set(.HelveticaNeue, size: 10))
              .foregroundColor(.set(.blueProject))
          })
          .frame(width: geo.size.width - 32, height: 23, alignment: .center)
          .modifier(BorderView(corner: 3, color: .set(.blueProject), width: 1))
          Spacer(minLength: viewModel.constraints.couponeButtonTopPadding)
        }
        .frame(width: geo.size.width, height: geo.size.height, alignment: .center)
      }
      .frame(width: geo.size.width, height: geo.size.height, alignment: .center)
      .background(RoundedRectangle(cornerRadius: 5).foregroundColor(.white))
      .clipShape(RoundedRectangle(cornerRadius: 5))
      .shadow(color: Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)), radius: 10)
      
      
    }
  }
}

struct CouponeCellViewPreviews: PreviewProvider {
  static var previews: some View {
    MainCouponeCellView()
      //iPhone X
      //.previewLayout(.fixed(width: 182.97, height: 155.8))
      //iPhone MAX
      .previewLayout(.fixed(width: 202, height: 172))
  }
}
