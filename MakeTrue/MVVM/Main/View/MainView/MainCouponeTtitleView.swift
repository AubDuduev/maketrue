
import SwiftUI

//MARK: - MainCouponeTtitleView
struct MainCouponeTtitleView: View {
  
  @EnvironmentObject private var viewModel: MainViewModel
  
  var body: some View {
    GeometryReader { geo in
      let _ = geo.size
      VStack {
        HStack {
          Text("Горячие предложения для Вас")
            .font(Font.set(.HelveticaNeue, size: viewModel.constraints.couponeTitleTextSize))
          Image.set(.mainTextFire)
            .resizable()
            .frame(width: 12, height: 16, alignment: .leading)
          Spacer()
          Button(action: {}, label: {
            Text("Посмотреть все")
              .font(Font.set(.HelveticaNeue, size: viewModel.constraints.couponeTitleTextSize))
              .foregroundColor(.set(.blueProject))
          }).padding(.trailing, 16.5)
        }
        .padding(.leading, 11)
        .frame(height: 16)
      }
    }
  }
}
struct MainCouponeTtitleView_Previews: PreviewProvider {
  static var previews: some View {
    MainCouponeTtitleView()
      .previewLayout(.fixed(width: 375, height: 14))
  }
}
