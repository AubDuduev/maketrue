
import UIKit
import SwiftUI

struct MainViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let MainVC = MainViewController()
    return MainVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
