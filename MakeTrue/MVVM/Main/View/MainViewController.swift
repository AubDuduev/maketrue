import UIKit

class MainViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: MainViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = MainViewModel(viewController: self)
  }
}
