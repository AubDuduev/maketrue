
import UIKit

enum MainModel {
	
	case loading
	case getData
	case errorHandler(MainViewData?)
	case presentData(MainViewData)
	
	
}

