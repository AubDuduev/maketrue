import UIKit

class iPhone7MainConstraints: MainConstraintoble  {
  //Header image
  public var headerImageHeight       : CGFloat = 270
  //Categories
  public var categiriesHeight        : CGFloat = 170
  public var categiriesHStackPaddingT: CGFloat {
    get {
      return ((categiriesHeight - categiriesIconSquare) / 3) * 2
    }
  }
  public var categiriesHStackPaddingB: CGFloat {
    get {
      return ((categiriesHeight - categiriesIconSquare) / 3)
    }
  }
  public var categiriesHStackLPadding: CGFloat {
    get {
      let commonSquare  = (categiriesIconSquare * 3)
      let commonPadding = CGFloat(12 * 2)
      let commonScreenW = UIScreen.main.bounds.width
      let result = ((commonScreenW - commonSquare) - commonPadding) / 2
      return result
    }
  }
  public var categiriesIconSquare    : CGFloat = 98
  //Coupone title
  public var couponeTitleHeight      : CGFloat = 16
  public var couponeTitleTextSize    : CGFloat = 14
  //Coupone
  public var couponeButtonTopPadding : CGFloat = 14
  public var couponeCellImageHight   : CGFloat = 81
  public var couponeHeight           : CGFloat = 152
  public var couponeWidth            : CGFloat = 182
  public var couponeScrollHeight     : CGFloat = 197
  public var couponeScrollVPadding   : CGFloat = 25
  //Search and date
  public var searchViewHeight        : CGFloat = 44
  public var dateViewHeight          : CGFloat = 31
  public var dateViewWidth           : CGFloat = 183
  public var spacingDateSearchHeight : CGFloat = 23
  public var dateSearchViewsHeight   : CGFloat = 100
  public var dateSearchViewsPositionY: CGFloat = 195
  //Common
  var commonViewPaddingB             : CGFloat = 100
}
