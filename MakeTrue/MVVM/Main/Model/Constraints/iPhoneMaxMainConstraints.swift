
import UIKit

class iPhoneMaxMainConstraints: MainConstraintoble  {
  //Header image
  public var headerImageHeight       : CGFloat = 323
  //Categories
  public var categiriesHeight        : CGFloat = 251
  public var categiriesHStackPaddingT: CGFloat {
    get {
      return (categiriesHeight - categiriesIconSquare) / 2
    }
  }
  public var categiriesHStackPaddingB: CGFloat {
    get {
      return (categiriesHeight - categiriesIconSquare) / 2
    }
  }
  public var categiriesHStackLPadding: CGFloat {
    get {
      let commonSquare  = (categiriesIconSquare * 3)
      let commonPadding = CGFloat(12 * 2)
      let commonScreenW = UIScreen.main.bounds.width
      let result = ((commonScreenW - commonSquare) - commonPadding) / 2
      return result
    }
  }
  public var categiriesIconSquare    : CGFloat = 110
  //Coupone title
  public var couponeTitleHeight      : CGFloat = 16
  public var couponeTitleTextSize    : CGFloat = 14
  //Coupone
  public var couponeButtonTopPadding : CGFloat = 14
  public var couponeCellImageHight   : CGFloat = 90
  public var couponeHeight           : CGFloat = 172
  public var couponeWidth            : CGFloat = 202
  public var couponeScrollHeight     : CGFloat = 197
  public var couponeScrollVPadding   : CGFloat = 25
  //Search and date
  public var searchViewHeight        : CGFloat = 48
  public var dateViewHeight          : CGFloat = 35
  public var dateViewWidth           : CGFloat = 203
  public var spacingDateSearchHeight : CGFloat = 26
  public var dateSearchViewsHeight   : CGFloat = 123
  public var dateSearchViewsPositionY: CGFloat = 275
  //Common
  var commonViewPaddingB             : CGFloat = 100
}
