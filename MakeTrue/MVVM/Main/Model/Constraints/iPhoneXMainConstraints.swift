
import UIKit

class iPhoneXMainConstraints: MainConstraintoble {
  
  //Header image
  public var headerImageHeight       : CGFloat = 292.57
  //Categories
  public var categiriesHStackPaddingT: CGFloat {
    get {
      return (categiriesHeight - categiriesIconSquare) / 2
    }
  }
  public var categiriesHStackPaddingB: CGFloat {
    get {
      return (categiriesHeight - categiriesIconSquare) / 2
    }
  }
  public var categiriesHStackLPadding: CGFloat {
    get {
      let commonSquare  = (categiriesIconSquare * 3)
      let commonPadding = CGFloat(12 * 2)
      let commonScreenW = UIScreen.main.bounds.width
      let result = ((commonScreenW - commonSquare) - commonPadding) / 2
      return result
    }
  }
  public var categiriesHeight        : CGFloat = 226.5
  public var categiriesIconSquare    : CGFloat = 99
  //Coupone title
  public var couponeTitleHeight      : CGFloat = 15
  public var couponeTitleTextSize    : CGFloat = 12
  //Coupone
  public var couponeButtonTopPadding : CGFloat = 11
  public var couponeCellImageHight   : CGFloat = 81.52
  public var couponeHeight           : CGFloat = 155.8
  public var couponeWidth            : CGFloat = 182.97
  public var couponeScrollHeight     : CGFloat = 177.94
  public var couponeScrollVPadding   : CGFloat = 22
  //Search and date
  public var searchViewHeight        : CGFloat = 48
  public var dateViewHeight          : CGFloat = 31.7
  public var dateViewWidth           : CGFloat = 183.8
  public var spacingDateSearchHeight : CGFloat = 23.55
  public var dateSearchViewsHeight   : CGFloat = 111.41
  public var dateSearchViewsPositionY: CGFloat = 245
  //Common
  var commonViewPaddingB             : CGFloat = 100
}
