
import UIKit
import Combine

class MainViewData: ObservableObject {
  
  @Published var name       = ""
  @Published var playLottie: Bool = false
}

