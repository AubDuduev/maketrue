
import Foundation
import Protocols
import SwiftUI
import Combine

class MainViewModel: VMManagers, ObservableObject {
	
	public var MainModel: MainModel = .loading {
		didSet{
			self.commonLogicMain()
		}
	}
  
  //MARK: - Public variable
  public var managers    : MainManagers!
  public var VC          : MainViewController!
  public var viewData    = MainViewData()
  public var view        : MainView!
  public var cancellable : AnyCancellable!
  public var constraints : MainConstraintoble {
    get {
      return self.managers.setup.choiceDevice()
    }
  }
  
  public func commonLogicMain(){
    
    switch self.MainModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init(){
    self.managers = MainManagers(viewModel: self)
  }
}
//MARK: - Initial
extension MainViewModel {
  
  convenience init(viewController: MainViewController) {
    self.init()
    self.VC       = viewController
    self.managers = MainManagers(viewModel: self)
  }
}
