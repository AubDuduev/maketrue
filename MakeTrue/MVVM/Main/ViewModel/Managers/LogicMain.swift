import Foundation
import Protocols
import SwiftUI

class LogicMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func printLogic(size: GeometryProxy) -> Int {
    
    print(size.frame(in: .global).minY)
    print(size)
  return 0
  }
  public func play(view: MainIconCategoryView){
    view.lottieView.lottieSetup.play()
  }
}
//MARK: - Initial
extension LogicMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}
