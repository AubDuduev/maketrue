
import UIKit
import Protocols
import SwiftUI
//import SnapKit


class SetupMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func hostingVC(){
//    let view     = MainView(viewData: self.VM.viewData)
//    let hostingVC = UIHostingController(rootView: view)
//    self.VM.VC.view.addSubview(hostingVC.view)
//    self.VM.VC.addChild(hostingVC)
//    hostingVC.view.snp.makeConstraints { (hostingVCView) in
//      hostingVCView.edges.equalTo(self.VM.VC.view)
//    }
  }
  public func cancellableViewData(){
//    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
//      print(name)
//    })
  }
  public func lottieSetup(view: MainIconCategoryView){
    let indexRow = view.indexRow ?? 0
    view.lottieView.setupLottie(indexRow: indexRow)
  }
  public func choiceDevice() -> MainConstraintoble {
    let mainSetConstraint = MainSetConstraint()
    return mainSetConstraint.setConstraint()
  }
}
//MARK: - Initial
extension SetupMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}


