
import UIKit
import Foundation

class MainSetConstraint {
  
  
  public func setConstraint() -> MainConstraintoble {
    print(GDScreenDevice.get().rawValue, "current device")
    
    switch GDScreenDevice.get() {
      
      //1 - iPhone 5
      case .iPhone5:
        return iPhone5MainConstraints()
      //2 - iPhone 7
      case .iPhone7:
        return iPhone7MainConstraints()
      //3 - iPhone Plus
      case .iPhonePlus:
        return iPhonePlusMainConstraints()
      //4 - iPhone X
      case .iPhoneX:
        return iPhoneXMainConstraints()
      //5 - iPhone 12
      case .iPhone12:
        return iPhone12MainConstraints()
      //6 - iPhone MAX
      case .iPhoneMax:
        return iPhoneMaxMainConstraints()
      //7 - iPhone 12 MAX
      case .iPhone12Max:
        return iPhone12MaxMainConstraints()
    }
  }
}




