
import UIKit
import Protocols

class RouterMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  
}
//MARK: - Initial
extension RouterMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}



