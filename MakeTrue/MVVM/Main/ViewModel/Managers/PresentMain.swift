
import UIKit
import Protocols

class PresentMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  
}
//MARK: - Initial
extension PresentMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}

