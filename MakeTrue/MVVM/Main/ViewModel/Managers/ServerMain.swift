import UIKit
import Protocols

class ServerMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  
}
//MARK: - Initial
extension ServerMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}


