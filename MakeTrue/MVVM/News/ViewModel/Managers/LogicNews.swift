import Foundation
import Protocols

class LogicNews: VMManager {
  
  //MARK: - Public variable
  public var VM: NewsViewModel!
  
  
}
//MARK: - Initial
extension LogicNews {
  
  //MARK: - Inition
  convenience init(viewModel: NewsViewModel) {
    self.init()
    self.VM = viewModel
  }
}
