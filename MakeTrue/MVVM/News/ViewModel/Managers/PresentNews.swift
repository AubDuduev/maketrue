
import UIKit
import Protocols

class PresentNews: VMManager {
  
  //MARK: - Public variable
  public var VM: NewsViewModel!
  
  
}
//MARK: - Initial
extension PresentNews {
  
  //MARK: - Inition
  convenience init(viewModel: NewsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

