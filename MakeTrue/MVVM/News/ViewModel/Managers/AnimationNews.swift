import UIKit
import Protocols

class AnimationNews: VMManager {
  
  //MARK: - Public variable
  public var VM: NewsViewModel!
  
  
}
//MARK: - Initial
extension AnimationNews {
  
  //MARK: - Inition
  convenience init(viewModel: NewsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

