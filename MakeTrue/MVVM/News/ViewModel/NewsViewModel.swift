
import Foundation
import Protocols
import SwiftUI
import Combine

class NewsViewModel: VMManagers {
	
	public var NewsModel: NewsModel = .loading {
		didSet{
			self.commonLogicNews()
		}
	}
  
  //MARK: - Public variable
  public var managers    : NewsManagers!
  public var VC          : NewsViewController!
  public var viewData    = NewsViewData()
  public var view        : NewsView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicNews(){
    
    switch self.NewsModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension NewsViewModel {
  
  convenience init(viewController: NewsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = NewsManagers(viewModel: self)
  }
}
