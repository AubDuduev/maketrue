
import UIKit

enum NewsModel {
	
	case loading
	case getData
	case errorHandler(NewsViewData?)
	case presentData(NewsViewData)
	
	
}

