
import Foundation
import Protocols

class NewsManagers: VMManagers {
  
  let setup    : SetupNews!
  let server   : ServerNews!
  let present  : PresentNews!
  let logic    : LogicNews!
  let animation: AnimationNews!
  let router   : RouterNews!
  
  init(viewModel: NewsViewModel) {
    
    self.setup     = SetupNews(viewModel: viewModel)
    self.server    = ServerNews(viewModel: viewModel)
    self.present   = PresentNews(viewModel: viewModel)
    self.logic     = LogicNews(viewModel: viewModel)
    self.animation = AnimationNews(viewModel: viewModel)
    self.router    = RouterNews(viewModel: viewModel)
  }
}

