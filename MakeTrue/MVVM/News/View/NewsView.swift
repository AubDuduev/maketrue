
import UIKit
import SwiftUI
import Combine

struct NewsView: View {
  
  @State private var viewModel = NewsViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: NewsViewData
  
  var body: some View {
    
    VStack {
      
    }
    .onAppear(){
     
    }
  }
}

struct NewsPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = NewsViewData()
    NewsView(viewData: viewData)
  }
}
