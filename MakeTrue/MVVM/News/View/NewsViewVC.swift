
import UIKit
import SwiftUI

struct NewsViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let NewsVC = NewsViewController()
    return NewsVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
