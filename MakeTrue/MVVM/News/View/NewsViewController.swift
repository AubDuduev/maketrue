import UIKit

class NewsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: NewsViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = NewsViewModel(viewController: self)
  }
}
