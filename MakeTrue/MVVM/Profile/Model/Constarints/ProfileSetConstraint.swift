
import UIKit
import Foundation

class ProfileSetConstraint {
  
  
  public func setConstraint() -> ProfileConstraintoble {
    print(GDScreenDevice.get().rawValue, "current device")
    
    switch GDScreenDevice.get() {
      
      //1 - iPhone 5
      case .iPhone5:
        return iPhone5ProfileConstraints()
      //2 - iPhone 7
      case .iPhone7:
        return iPhone7ProfileConstraints()
      //3 - iPhone Plus
      case .iPhonePlus:
        return iPhonePlusProfileConstraints()
      //4 - iPhone X
      case .iPhoneX:
        return iPhoneXProfileConstraints()
      //5 - iPhone 12
      case .iPhone12:
        return iPhone12ProfileConstraints()
      //6 - iPhone MAX
      case .iPhoneMax:
        return iPhoneMaxProfileConstraints()
      //7 - iPhone 12 MAX
      case .iPhone12Max:
        return iPhone12MaxProfileConstraints()
    }
  }
}




