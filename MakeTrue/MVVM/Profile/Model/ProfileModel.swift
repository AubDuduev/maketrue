
import UIKit

enum ProfileModel {
	
	case loading
	case getData
	case errorHandler(ProfileViewData?)
	case presentData(ProfileViewData)
	
	
}

