
import Foundation
import Protocols

class ProfileManagers: VMManagers {
  
  let setup    : SetupProfile!
  let server   : ServerProfile!
  let present  : PresentProfile!
  let logic    : LogicProfile!
  let animation: AnimationProfile!
  let router   : RouterProfile!
  
  init(viewModel: ProfileViewModel) {
    
    self.setup     = SetupProfile(viewModel: viewModel)
    self.server    = ServerProfile(viewModel: viewModel)
    self.present   = PresentProfile(viewModel: viewModel)
    self.logic     = LogicProfile(viewModel: viewModel)
    self.animation = AnimationProfile(viewModel: viewModel)
    self.router    = RouterProfile(viewModel: viewModel)
  }
}

