
import UIKit
import Combine

class ProfileViewData: ObservableObject {
  
  @Published var name   = "Aleksandr"
  @Published var number = "+7999333557"
  
  public var profileDates: [ProfileData] {
    get {
      var profileDates = [ProfileData]()
      for index in 0..<ProfileSectionName.allCases.count {
        let name = ProfileSectionName.allCases[index]
        let icon = ProfileSectionIcon.allCases[index]
        let profileData = ProfileData(id: UUID(), name: name, icon: icon)
        profileDates.append(profileData)
      }
    return profileDates
    }
  }
}

struct ProfileData: Hashable, Identifiable {
  
  var id = UUID()
  let name: ProfileSectionName!
  let icon: ProfileSectionIcon!
}
enum ProfileSectionName: String, CaseIterable {

  case personalInfo = "Личная информация"
  case interests    = "Мои интересы"
  case subscribe    = "Мои подписки"
  case setting      = "Настройки"
  case signOut      = "Выход из аккаунта"
}

enum ProfileSectionIcon: String, CaseIterable {
  
  case personalnfoProfile
  case interestsProfile
  case subscribeProfile
  case settingProfile
  case signOutProfile
}
