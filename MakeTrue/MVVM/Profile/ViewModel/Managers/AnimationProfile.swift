import UIKit
import Protocols

class AnimationProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  
}
//MARK: - Initial
extension AnimationProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}

