
import UIKit
import Protocols
import SwiftUI
//import SnapKit


class SetupProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  public func hostingVC(){
//    let view     = ProfileView(viewData: self.VM.viewData)
//    let hostingVC = UIHostingController(rootView: view)
//    self.VM.VC.view.addSubview(hostingVC.view)
//    self.VM.VC.addChild(hostingVC)
//    hostingVC.view.snp.makeConstraints { (hostingVCView) in
//      hostingVCView.edges.equalTo(self.VM.VC.view)
//    }
  }
  public func cancellableViewData(){
//    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
//      print(name)
//    })
  }
  public func choiceDevice() -> ProfileConstraintoble {
    let setConstraint = ProfileSetConstraint()
    return setConstraint.setConstraint()
  }
}
//MARK: - Initial
extension SetupProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}


