import UIKit
import Protocols

class ServerProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  
}
//MARK: - Initial
extension ServerProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}


