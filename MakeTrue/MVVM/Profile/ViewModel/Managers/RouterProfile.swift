
import UIKit
import Protocols
import SwiftUI

class RouterProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  
  enum ProfilePushVC {
    case Interests
  }
  
  
  public func pushVC(type: ProfileSectionName) -> some View {
    switch type {
      case .interests:
        let viewData = InterestsViewData()
        return AnyView(InterestsView(viewData: viewData))
      case .personalInfo:
        print("personalInfo")
        return AnyView(EmptyView())
      case .subscribe:
        print("subscribe")
        return AnyView(EmptyView())
      case .setting:
        print("setting")
        let viewData = SettingViewData()
        return AnyView(SettingView(viewData: viewData))
      case .signOut:
        print("signOut")
        return AnyView(ActionRestarauntView(viewModel: BrandCardViewModel()))
    }
  }
}
//MARK: - Initial
extension RouterProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}



