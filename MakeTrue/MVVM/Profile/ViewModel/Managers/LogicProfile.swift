import Foundation
import Protocols
import SwiftUI

class LogicProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  public func changePhotoAvatar(view: AvatarProfileView, response: Int){
    switch response {
      case 0:
        view.typeResource = .library
        view.isImagePicker.toggle()
      case 1:
        view.typeResource = .camera
        view.isImagePicker.toggle()
      case 2:
        break
      default:
        break
    }
  }
  public func changePresentAvatarImage(view: AvatarProfileView) -> some View {
    if view.changePhoto == nil {
      return AnyView(Image.set(.profileAddPhoto)
                      .resizable()
                      .frame(width : VM.constraints.profilePhotoSquare,
                             height: VM.constraints.profilePhotoSquare))
    } else {
      return AnyView(Image(uiImage: view.changePhoto)
                      .resizable()
                      .aspectRatio(contentMode: .fill)
                      .frame(width : VM.constraints.profilePhotoSquare,
                             height: VM.constraints.profilePhotoSquare)
                      .clipsToBounds(VM.constraints.profilePhotoSquare / 2))
    }
  }
}
//MARK: - Initial
extension LogicProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}
