
import UIKit
import Protocols

class PresentProfile: VMManager {
  
  //MARK: - Public variable
  public var VM: ProfileViewModel!
  
  
}
//MARK: - Initial
extension PresentProfile {
  
  //MARK: - Inition
  convenience init(viewModel: ProfileViewModel) {
    self.init()
    self.VM = viewModel
  }
}

