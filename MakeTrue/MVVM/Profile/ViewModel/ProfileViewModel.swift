
import Foundation
import Protocols
import SwiftUI
import Combine

class ProfileViewModel: VMManagers, ObservableObject {
	
	public var ProfileModel: ProfileModel = .loading {
		didSet{
			self.commonLogicProfile()
		}
	}
  
  //MARK: - Public variable
  public var managers    : ProfileManagers!
  public var VC          : ProfileViewController!
  public var viewData    = ProfileViewData()
  public var view        : ProfileView!
  public var cancellable : AnyCancellable!
  public var constraints : ProfileConstraintoble {
    get {
      return self.managers.setup.choiceDevice()
    }
  }
  
  public func commonLogicProfile(){
    
    switch self.ProfileModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init() {
    self.managers = ProfileManagers(viewModel: self)
  }
}
//MARK: - Initial
extension ProfileViewModel {
  
  convenience init(viewController: ProfileViewController) {
    self.init()
    self.VC       = viewController
    self.managers = ProfileManagers(viewModel: self)
  }
}

protocol ProfileConstraitoble {
  
  //Avatar
  var avatarProfileViewHeight: CGFloat { get set }
}
