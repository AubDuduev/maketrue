
import SwiftUI

struct ProfileSectionView: View {
  
  public let profileData: ProfileData!
  
  @EnvironmentObject public var viewModel: ProfileViewModel
  
  @State var isNavigation  = false
  @State var isAlertSheet  = false
 
  var body: some View {
    
    GeometryReader() { geo in
      let commonSize = geo.size
      
      VStack(spacing: 0) {
        //MARK: - icon, name, arrow right
        HStack(spacing: 10) {
          //MARK: - Image icon
          Group {
            Image.set(profileData.icon.rawValue)
              .resizable()
              .renderingMode(.original)
              .aspectRatio(contentMode: .fit)
              .padding(.top, 16)
              .padding(.bottom, 13)
              .padding(.leading, 5)
          }
          .frame(width : commonSize.height,
                 height: commonSize.height,
                 alignment: .center)
          //MARK: - Name
          ZStack {
            let destination = self.viewModel.managers.router.pushVC(type: profileData.name)
            NavigationLink("", destination: destination, isActive: self.$isNavigation)
            Button(action: {
              switch self.profileData.name {
                case .signOut:
                  self.isAlertSheet.toggle()
                default:
                  self.isNavigation.toggle()
              }
             
            },
            label: {
              Text(profileData.name.rawValue)
                .font(Font.set(.latoRegular, size: 14.5))
            })
          }
          .padding(.top, 19)
          .padding(.bottom, 13)
          
          //MARK: - Spacer
          Spacer()
          //MARK: - Arrow right
          Group {
          Image.set(.arrowRightProfile)
            .resizable()
            .renderingMode(.original)
            .aspectRatio(contentMode: .fit)
            .padding(.bottom, 14)
            .padding(.top, 22)
          }
            .frame(width : commonSize.height,
                   height: commonSize.height,
                   alignment: .center)
        }
        //MARK: - Gray line
        Divider().frame(width: commonSize.width - 32, height: 1, alignment: .center)
      }
      .alertSheet(.information, .signOutToAccount, .signOutAccount, $isAlertSheet){ index in
        print(index)
      }
      
    }
    .frame(height: self.viewModel.constraints.profileSectionViewHeight, alignment: .center)
      
  }
}

struct ProfileSectionView_Previews: PreviewProvider {

  static var previews: some View {

    ProfileSectionView(profileData: ProfileData(id  : UUID(),
                                                name: .personalInfo,
                                                icon: .personalnfoProfile))
      .environmentObject(ProfileViewModel())
      .previewLayout(.fixed(width: 428, height: 55))
  }
}


