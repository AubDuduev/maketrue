
import UIKit
import SwiftUI
import Combine

struct ProfileView: View {
  
  @State private var viewModel = ProfileViewModel()
  @State private var pushVC: RouterProfile.ProfilePushVC! = .Interests
  @State public var alertIsPresented = true

  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData = ProfileViewData()
  
  var body: some View {
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack(spacing: 0) {
        //MARK: - Avatar
        AvatarProfileView()
          .environmentObject(self.viewData)
          .environmentObject(self.viewModel)
        //MARK: - List menu
        ForEach(viewData.profileDates, id: \.self) { profileData in
          VStack {
            //MARK: - ProfileSectionView
            ProfileSectionView(profileData: profileData)
              .environmentObject(self.viewModel)
          }
        }
        Spacer()
      }
      .frame(width: commonSize.width, height: commonSize.height, alignment: .center)
      .edgesIgnoringSafeArea(.all)
    }
    .edgesIgnoringSafeArea(.all)
  }
}

struct ProfilePreviews: PreviewProvider {
  static var previews: some View {
    let viewData = ProfileViewData()
    ProfileView(viewData: viewData)
      //      //iPhone 5
      //      .previewLayout(.fixed(width: 320.0, height: 568.0))
      //      //iPhone 7
      //      .previewLayout(.fixed(width: 375.0, height: 667.0))
      //      //iPhone Plus
      //      .previewLayout(.fixed(width: 414.0, height: 736.0))
      //      //iPhone X
      //      .previewLayout(.fixed(width: 375.0, height: 812.0))
      //      //iPhone X Max
      //      .previewLayout(.fixed(width: 414.0, height: 896.0))
      //iPhone 12
      .previewLayout(.fixed(width: 390.0, height: 844.0))
  }
}

//
