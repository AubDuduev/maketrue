
import SwiftUI

struct AvatarProfileView: View {
  
  @EnvironmentObject var viewModel: ProfileViewModel
  @EnvironmentObject var viewData : ProfileViewData
  
  @State var changePhoto: UIImage!
  @State var isImagePicker = false
  @State var isAlertPhoto  = false
  @State var typeResource  : GDImagePickerVC.TypeResources!
  
  var body: some View {
    
    GeometryReader() { geo in
      let commonSize = geo.size
      VStack(spacing: 0) {
        Spacer(minLength: viewModel.constraints.profilePhotoPeddingTop)
        Button(action: {
          self.isAlertPhoto.toggle()
        }, label: {
          //Выбираем что отобразить кнопку или выбраное фото
          self.viewModel.managers.logic.changePresentAvatarImage(view: self)
        })
        Text(self.viewData.name)
          .font(Font.set(.latoRegular, size: 14))
          .padding(.top, 13)
        Text(self.viewData.number)
          .font(Font.set(.HelveticaNeue, size: 14))
          .padding(.top, 2)
        Spacer()
      }
      //Вызов пикера с библиотекой/камерой
      .sheet(isPresented: self.$isImagePicker, content: {
        GDImagePickerVC(image: $changePhoto, typeResource: self.$typeResource)
      })
      //Алерт для выбора как изменить фото на
      .alertSheet(.information, .addPhotoChange, .addPhotoChange, self.$isAlertPhoto, completion: { (response) in
        self.viewModel.managers.logic.changePhotoAvatar(view: self, response: response)
      })
      .frame(width    : commonSize.width,
             height   : viewModel.constraints.avatarProfileViewHeight,
             alignment: .center)
    }
    .frame(height: viewModel.constraints.avatarProfileViewHeight, alignment: .center)
  }
}

struct AvatarProfileView_Previews: PreviewProvider {
  static var previews: some View {
    AvatarProfileView()
      .environmentObject(ProfileViewData())
      .environmentObject(ProfileViewModel())
      .previewLayout(.fixed(width: 428, height: 282))
  }
}


