
import UIKit
import SwiftUI

struct ProfileViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let ProfileVC = ProfileViewController()
    return ProfileVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
