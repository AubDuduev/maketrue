import UIKit

class ProfileViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: ProfileViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = ProfileViewModel(viewController: self)
  }
}
