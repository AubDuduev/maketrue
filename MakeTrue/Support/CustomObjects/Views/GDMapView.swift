
import MapKit
import SwiftUI

struct GDMapView: UIViewRepresentable {
  
  typealias UIViewControllerType = MKMapView
  
  func updateUIView(_ uiView: MKMapView, context: Context) {
    
  }
  
  public func makeUIView(context: Context) -> MKMapView {
    let mapViewVC = MKMapView()
    return mapViewVC
  }
  
}
