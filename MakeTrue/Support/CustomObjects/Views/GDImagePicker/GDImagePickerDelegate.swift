
import UIKit
import SwiftUI
import Photos

class GDImagePickerDelegate: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
  
  
  public var imagePicker: UIImagePickerController!
  
  
  private func library(){
    if !UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
      PHPhotoLibrary.requestAuthorization({ [weak self] (status) in
        guard let self = self else { return }
        if status == .authorized {
          DispatchQueue.main.async {
            self.imagePicker.sourceType = .photoLibrary
          }
        }
        else if status == .denied {
          
        }
      })
    } else {
      imagePicker.sourceType = .photoLibrary
    }
  }
  private func camera(){
    if !UIImagePickerController.isSourceTypeAvailable(.camera) {
      PHPhotoLibrary.requestAuthorization({ [weak self] (status) in
        guard let self = self else { return }
        if status == .authorized {
          DispatchQueue.main.async {
            self.imagePicker.sourceType = .camera
          }
        }
        else if status == .denied {
          
        }
      })
    } else {
      imagePicker.sourceType = .camera
    }
  }
  
  public func setup(delegate: GDImagePickerDelegate, type: GDImagePickerVC.TypeResources){
    imagePicker            = UIImagePickerController()
    imagePicker.delegate   = delegate
    imagePicker.isEditing  = true
    imagePicker.modalPresentationStyle = .fullScreen
    imagePicker.modalTransitionStyle   = .crossDissolve
    switch type {
      case .camera:
        self.camera()
      case .library:
        self.library()
      
    }
  }
  
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    if let image = info[.editedImage] as? UIImage {
      self.parent.image = image
      
    } else if let image = info[.originalImage] as? UIImage {
      self.parent.image = image
    }
    self.parent.dismiss.wrappedValue.dismiss()
  }
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    self.parent.dismiss.wrappedValue.dismiss()
  }
  let parent: GDImagePickerVC!
  
  init(parent: GDImagePickerVC) {
    self.parent = parent
  }
}
