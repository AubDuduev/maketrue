
import UIKit
import SwiftUI
import Photos

struct GDImagePickerVC: UIViewControllerRepresentable {
  
  @Binding var image       : UIImage!
  @Binding var typeResource: TypeResources!
  
  @Environment(\.presentationMode) var dismiss
  
  public func makeUIViewController(context: Context) -> UIImagePickerController {
    let imagePickerVC = GDImagePickerDelegate(parent: self)
    imagePickerVC.setup(delegate: context.coordinator, type: self.typeResource)
    return imagePickerVC.imagePicker
  }
  
  public func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
    
  }
  
  func makeCoordinator() -> GDImagePickerDelegate {
    GDImagePickerDelegate(parent: self)
  }

  enum TypeResources {
    case library
    case camera
  }
}

