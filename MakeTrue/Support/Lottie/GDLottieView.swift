
import UIKit
import SwiftUI
import SnapKit
import Combine
import Lottie

final class GDLottieView: UIViewRepresentable {
  
  public var lottieSetup: GDLottieSetup!
  
  private let view = UIView(frame: .zero)
  
  func updateUIView(_ uiView: UIView, context: Context) {
    
  }
  
  func makeUIView(context: Context) -> UIView {
    
  return view
  }
  
  public func setupLottie(indexRow: Int){
    self.lottieSetup?.remove()
    let name = LottieCategories.allCases[indexRow].rawValue
    self.lottieSetup = GDLottieSetup(view: self.view)
    self.lottieSetup.added(name: name, loopMode: .playOnce)
    self.lottieSetup.setup()
  }
  
  public func setupLottie(lottieFile: GDLottieSetup.LottieName, loopMode: LottieLoopMode = .playOnce){
    self.lottieSetup?.remove()
    let name = lottieFile.rawValue
    self.lottieSetup = GDLottieSetup(view: self.view)
    self.lottieSetup.added(name: name, loopMode: loopMode)
    self.lottieSetup.setup()
  }
 
  enum LottieCategories: String, CaseIterable  {
    case mainRestoraunt
    case mainHotel
    case mainMagaz
  }
}

