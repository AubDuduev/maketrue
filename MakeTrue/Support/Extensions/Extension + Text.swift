import SwiftUI

extension Text {
  
  static func set(_ name: TextNames) -> Text {
    
    return Text(name.rawValue)
  }
  
  static func set(_ name: String) -> Text {
    
    return Text(name)
  }
  
  enum TextNames: String {
    
    case mainPlaneIcon = "Планируете поездку рядом?"
    //NavBar
    case navBarInterests = "Мои интересы"
    case navBarSetting   = "Настройки"
  }
}
