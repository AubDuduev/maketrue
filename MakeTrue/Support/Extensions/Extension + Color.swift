
import SwiftUI

extension Color {
  
  static func set(_ name: ColorName) -> Color {
    
    return Color(name.rawValue)
  }
  
  enum ColorName: String {
    
    case blueProject
    case orangeProject
    case shadowBC
    case balckOpacity
    case shadowButtonContactBC
    case arrowDownBarndCard
    case settingButton
  }
}


