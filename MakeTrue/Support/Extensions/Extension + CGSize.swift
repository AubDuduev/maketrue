
import UIKit

extension CGSize {
  
   func center(_ type: Center) -> CGFloat {
    
    switch type {
      
      case .top:
        return -(self.height / 2)
      case .bottom:
        return (self.height / 2)
      case .left:
        return -(self.width / 2)
      case .right:
        return (self.width / 2)
    }
  }
  
  enum Center {
    case top
    case bottom
    case left
    case right
  }
}

