
import SwiftUI

extension View {
  
  func navigationBarColor(bg: UIColor = .white, largeTitle: UIColor = .white, title: UIColor = .white) -> some View {
    self.modifier(NavigationBarModifier(bg: bg, largeTitle: largeTitle, title: title))
  }
  
}

