
import SwiftUI

extension Font {
  
  static func set(_ name: ImageName, size: CGFloat = 10) -> Font {
    
    return Font.custom(name.rawValue, size: size)
  }
  
  enum ImageName: String {
    
    case HelveticaNeue
    case latoRegular = "Lato-Regular"
    case LatoBlack   = "Lato-Black"
  }
}
