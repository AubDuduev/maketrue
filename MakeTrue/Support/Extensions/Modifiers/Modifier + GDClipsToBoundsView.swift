
import SwiftUI

struct GDClipsToBoundsView: ViewModifier {
  
  var cornerRadius: CGFloat!
  
  func body(content: Content) -> some View {
    content.clipShape(RoundedRectangle(cornerRadius: cornerRadius))
  }
  
  init(cornerRadius: CGFloat) {
    self.cornerRadius = cornerRadius
  }
}
//Stroke        - чертит 50/50
//Strike border - чертит во внутрь

extension View {
  
  func clipsToBounds(_ cornerRadius: CGFloat) -> some View {
    return self.modifier(GDClipsToBoundsView(cornerRadius: cornerRadius))
  }
}
