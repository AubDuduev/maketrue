
import SwiftUI

struct GDAlertSheet: ViewModifier {
  
  @Binding public var isPresent: Bool
  
  public let title       : Texts.Title
  public let message     : Texts.Message
  public let actionType  : ActionTypes!
  public var completion  : Clousure<Int>!
  
  func body(content: Content) -> some View {
    content.actionSheet(isPresented: self.$isPresent, content: {
      ActionSheet(title  : Text(title.rawValue),
                  message: Text(message.rawValue),
                  buttons: self.actions())
    })
  }
  
  private func actions() -> [ActionSheet.Button] {
    let countButton   = self.actionTypes(type: self.actionType)
    let createButtons = countButton.enumerated().map { index, title -> ActionSheet.Button in
      
      switch index {
        case (countButton.count - 1):
          return ActionSheet.Button.cancel(Text(title)) {
            completion?(index)
          }
        default:
          return ActionSheet.Button.default(Text(title)) {
            completion?(index)
          }
          
      }
      
    }
    return createButtons
  }
  
  private func actionTypes(type: ActionTypes) -> [String] {
    
    switch type {
      case .changeToLanguage:
        return GDChangeLanguage.allCases.compactMap({$0.rawValue})
      case .deleteToAccount:
        return GDDeleteLanguage.allCases.compactMap({$0.rawValue})
      case .signOutAccount:
        return GDSignOut.allCases.compactMap({$0.rawValue})
      case .addPhotoChange:
        return GDPhotoChange.allCases.compactMap({$0.rawValue})
    }
  }
  enum ActionTypes {
    case changeToLanguage
    case deleteToAccount
    case signOutAccount
    case addPhotoChange
  }
}

enum GDChangeLanguage: String, CaseIterable {
  
  case russia = "Русский"
  case usa    = "Английский"
  case cancel = "Отмена"
}

enum GDDeleteLanguage: String, CaseIterable {
  
  case delete = "Удалить"
  case cancel = "Отмена"
}

enum GDSignOut: String, CaseIterable {
  
  case signOut = "Выйти"
  case cancel  = "Отмена"
}
enum GDPhotoChange: String, CaseIterable {
  
  case library = "Библиотека"
  case photo   = "Фотопарат"
  case cancel  = "Отмена"
}

