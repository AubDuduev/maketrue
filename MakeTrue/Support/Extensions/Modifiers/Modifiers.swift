
import SwiftUI

struct FrameStretch: ViewModifier {
  
  var geo: GeometryProxy!
  
  func body(content: Content) -> some View {
    let minGlobalY = geo.frame(in: .global).minY
    let commonSize = geo.size
    return content.frame(width: commonSize.width, height: (minGlobalY > 0) ? commonSize.height + minGlobalY : commonSize.height)
  }
}

struct OffsetStretch: ViewModifier {
  
  var geo: GeometryProxy!
  
  func body(content: Content) -> some View {
    let minGlobalY = geo.frame(in: .global).minY
    return content.offset(y: minGlobalY > 0 ? -minGlobalY : 0)
  }
}


struct BorderView: ViewModifier {
  
  var corner: CGFloat!
  var color : Color!   = .black
  var width : CGFloat! = 0
  
  func body(content: Content) -> some View {
    content.overlay(RoundedRectangle(cornerRadius: corner).stroke(color, lineWidth: width))
  }
  
  init(corner: CGFloat, color: Color = .black, width: CGFloat = 0) {
    self.corner = corner
    self.width  = width
    self.color  = color
  }
}



struct NavigationBarModifier: ViewModifier {
        
    var backgroundColor: UIColor?
    
  init(bg: UIColor, largeTitle: UIColor, title: UIColor) {
        self.backgroundColor = bg
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithTransparentBackground()
        coloredAppearance.backgroundColor          = .clear
        coloredAppearance.titleTextAttributes      = [.foregroundColor: title]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: largeTitle]
        
        UINavigationBar.appearance().standardAppearance   = coloredAppearance
        UINavigationBar.appearance().compactAppearance    = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
        UINavigationBar.appearance().tintColor = .white

    }
    
    func body(content: Content) -> some View {
        ZStack{
            content
            VStack {
                GeometryReader { geometry in
                    Color(self.backgroundColor ?? .clear)
                        .frame(height: geometry.safeAreaInsets.top)
                        .edgesIgnoringSafeArea(.top)
                    Spacer()
                }
            }
        }
    }
}


