
import SwiftUI

struct GDBackButton: ViewModifier {
  
  public var completion : Clousure<Int>!
  @Environment (\.presentationMode) var dismiss
  
  func body(content: Content) -> some View {
    content
      .navigationBarItems(leading: Button(action: {
        self.dismiss.wrappedValue.dismiss()
      }, label: {
        Image.set(.back)
          .frame(width: 30, height: 30, alignment: .center)
          .foregroundColor(.black)
      }))
      .navigationBarBackButtonHidden(true)
  }
}


extension View {
  
  func backButton() -> some View {
   return self.modifier(GDBackButton())
  }
}
