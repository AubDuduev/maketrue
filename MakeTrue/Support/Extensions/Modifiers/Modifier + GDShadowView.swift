
import SwiftUI

struct GDShadowView: ViewModifier {
  
  var corner: CGFloat!
  var radius: CGFloat!
  var color : Color!
  
  func body(content: Content) -> some View {
    content.background(RoundedRectangle(cornerRadius: corner).foregroundColor(.white).shadow(color: color, radius: radius))
  }
  
  init(corner: CGFloat = 0, radius: CGFloat, color: Color) {
    self.radius = radius
    self.color  = color
    self.corner = corner
  }
}

extension View {
  
  func shadow(corner: CGFloat = 0, radius: CGFloat, color: Color) -> some View {
    return  self.modifier(GDShadowView(corner: corner, radius: radius, color: color))
  }
}
