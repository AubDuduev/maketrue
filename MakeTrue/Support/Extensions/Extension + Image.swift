
import SwiftUI

extension Image {
  
  static func set(_ name: ImageNames) -> Image {
    
    return Image(name.rawValue)
  }
  
  static func set(_ name: String) -> Image {
    
    return Image(name)
  }
  
  static func set(_ style: Style) -> Image {
    
    return Image(systemName: style.rawValue)
  }
  enum Style: String {
    
    case back            = "chevron.backward"
    case checkmarkSquare = "checkmark.square"
    case square
    case checkmark
  }
  enum ImageNames: String {
    case mainIcon
    case couponeIcon
    case newsIcon
    case orderIcon
    case profileIcon
    case brandCardHeader
    case sotie
    case filter
    case loupe
    case downArrow
    case pin
    case moscowHeader
    case mainTextFire
    case mainIconRestoran
    case imageCouponeCell
    case mainPlaneIcon
    //Barnd card
    case arrowLeftBC
    case addSubscribeBC
    case fireCouponeBrandCard
    case couponesIconBarndCard
    case downArrowBrandCard
    case galleryPhotoBrandCard
    case imageBludeNewBrandCard
    case likeNewBrandCard
    case dislikeNewBrandCard
    case actionRestourantButton
    case orderDileveryBarndCard
    case orderTableBarndCard
    case metroBrandCard
    //Interests
    case sectionInterestsOne
    case sectionInterestsTwo
    case sectionInterestsThree
    case checkmarkInterests
    //Profile
    case profileAddPhoto
    case arrowRightProfile
    //Orders
    case noOrders
  }
}
