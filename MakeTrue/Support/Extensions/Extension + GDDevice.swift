import DeviceKit
import Foundation

class GDDevice {
  
  static let shared = GDDevice()
  
  public func type() ->  Device {
    let device = Device.current
   
    switch device {
      //1 - iPhone 5
      case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE, .iPhoneSE2, .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE), .simulator(.iPhoneSE2):
        return .iPhone5
      //2 - iPhone Plus
      case .iPhone6Plus, .iPhone6sPlus, .iPhone8Plus, .iPhone7Plus, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone8Plus), .simulator(.iPhone7Plus):
        return .iPhone6Plus
      //3 - iPhone 7
      case .iPhone6, .iPhone6s, .iPhone7, .iPhone8, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8):
        return .iPhone7
      //4 - iPhone X
      case .iPhoneX, .iPhoneXS, .iPhoneXR, .iPhone11, .iPhone11Pro, .iPhone12, .iPhone12Pro, .simulator(.iPhoneX), .simulator(.iPhoneXS), .simulator(.iPhoneXR), .simulator(.iPhone11), .simulator(.iPhone11Pro), .simulator(.iPhone12), .simulator(.iPhone12Pro):
        return .iPhoneX
      //5 - iPhone 11 Pro MAX
      case .iPhone11ProMax:
        return .iPhone11ProMax
      //6 - iPhone 11 MAX
      case .iPhoneXSMax, .simulator(.iPhone11ProMax), .simulator(.iPhoneXSMax):
        return .iPhone11ProMax
      //7 - iPhone 12 MAX
      case .iPhone12ProMax, .simulator(.iPhone12ProMax):
        return .iPhone12ProMax
      //8 - iPad 7
      case .simulator(.iPad7), .simulator(.iPadPro9Inch), .simulator(.iPadAir3):
        return .iPad7
      //9 - iPad Air
      case  .iPadAir, .iPadAir2, .iPadAir3:
        return .iPadAir
      //10 - iPadPro 9Inch
      case  .iPadPro9Inch:
        return .iPadPro9Inch
      //11 - iPadPro 11Inch
      case .iPadPro11Inch, .iPadPro11Inch2:
        return .iPadPro11Inch
      //12 - iPadPro 12Inch
      case  .iPadPro12Inch, .iPadPro12Inch2, .iPadPro12Inch3, .iPadPro12Inch4, .simulator(.iPadPro12Inch), .simulator(.iPadPro12Inch2), .simulator(.iPadPro12Inch3), .simulator(.iPadPro12Inch4):
        return .iPadPro12Inch
      //13 - iPad Mini
      case  .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4:
        return .iPadMini
      //14 - iPad 2
      case  .iPad2, .iPad3, .iPad4, .iPad5, .iPad6, .iPad7:
        return .iPad2
      //15 - Error
      default:
        return .unknown("unknown error")
    }
  }
}
