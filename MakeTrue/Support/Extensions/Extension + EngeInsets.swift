
import SwiftUI

extension View {
    
  public func engeInsetsZero() -> some View {
    self.listRowInsets(.init( top: 0, leading: 0,bottom: 0, trailing: 0))
  }
}


