import SwiftUI

extension View {
    
  func alertSheet(_ title: Texts.Title, _ message: Texts.Message, _ actionType: GDAlertSheet.ActionTypes, _ isPresent: Binding<Bool>, completion: @escaping Clousure<Int>) -> some View {
    
    let alertSheet = GDAlertSheet(isPresent : isPresent,
                                  title     : title,
                                  message   : message,
                                  actionType: actionType,
                                  completion: completion)
    return self.modifier(alertSheet)
  }
}


