

import UIKit

protocol MainConstraintoble {
  //Header image
  var headerImageHeight       : CGFloat { get set }
  //Header categories
  var categiriesHeight        : CGFloat { get set }
  var categiriesHStackPaddingT: CGFloat { get }
  var categiriesHStackPaddingB: CGFloat { get }
  var categiriesHStackLPadding: CGFloat { get }
  var categiriesIconSquare    : CGFloat { get set }
  //Coupone title
  var couponeTitleHeight      : CGFloat { get set }
  var couponeTitleTextSize    : CGFloat { get set }
  //Coupone
  var couponeCellImageHight   : CGFloat { get set }
  var couponeButtonTopPadding : CGFloat { get set }
  var couponeHeight           : CGFloat { get set }
  var couponeWidth            : CGFloat { get set }
  var couponeScrollHeight     : CGFloat { get set }
  var couponeScrollVPadding   : CGFloat { get }
  //Search and date
  var searchViewHeight         : CGFloat { get set }
  var dateViewHeight           : CGFloat { get set }
  var dateViewWidth            : CGFloat { get set }
  var spacingDateSearchHeight  : CGFloat { get set }
  var dateSearchViewsHeight    : CGFloat { get set }
  var dateSearchViewsPositionY : CGFloat { get set }
  //Common
  var commonViewPaddingB       : CGFloat { get set }
  
}

