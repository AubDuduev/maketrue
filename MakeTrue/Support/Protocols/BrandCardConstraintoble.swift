import UIKit

protocol BrandCardConstraintoble {
  
  //BCHeaderView
  var headerViewHeight         : CGFloat { get set }
  //BCCouponeView
  var couponeViewHeight        : CGFloat { get set }
  var couponeViewPaddingV      : CGFloat { get set }
  var couponeViewPaddingH      : CGFloat { get set }
  //BrandContactButtonsView
  var contactsButtonsViewHeight: CGFloat { get set }
  var contactsSquare           : CGFloat { get set }
  var contactsViewPaddingV     : CGFloat { get set }
  var contactsViewPaddingH     : CGFloat { get set }
  //BCDescriptionView
  var descriptionViewHeight    : CGFloat { get set }
  var descriptionTextPaddingV  : CGFloat { get set }
  var descriptionTextPaddingH  : CGFloat { get set }
  //BCMenuView
  var menuViewHeight           : CGFloat { get set }
  var menuViewPaddingV         : CGFloat { get set }
  var menuViewPaddingH         : CGFloat { get set }
  //BCOpeningHours
  var openingHoursHeight       : CGFloat { get set }
  var openingPaddingH          : CGFloat { get set }
  var openingPaddingT          : CGFloat { get set }
  //BCTagsView
  var tagsViewHeight           : CGFloat { get set }
  var tagsPaddingH             : CGFloat { get set }
  var tagsPaddingT             : CGFloat { get set }
  //BCNewsView
  var newsViewHeight           : CGFloat { get set }
  var newsPaddingH             : CGFloat { get set }
  var newsPaddingT             : CGFloat { get set }
  var newScrollPaddingL        : CGFloat { get set }
  var newScrollPaddingB        : CGFloat { get set }
  var newViewWidth             : CGFloat { get set }
  var newViewHeight            : CGFloat { get set }
  var newTimeViewHeight        : CGFloat { get set }
  var newImageViewHeight       : CGFloat { get set }
  var newDataViewHeight        : CGFloat { get set }
  //BCGalleryView
  var galleryViewWidth        : CGFloat { get set }
  var galleryViewHeight       : CGFloat { get set }
  var galleryMainPhotoWidth   : CGFloat { get set }
  var galleryMainPhotoHeight  : CGFloat { get set }
  var galleryPhotoViewWidth   : CGFloat { get set }
  var galleryPhotoViewHeight  : CGFloat { get set }
  var galleryPaddingH         : CGFloat { get set }
  var galleryPaddingT         : CGFloat { get set }
  var galleryPhotoPaddingV    : CGFloat { get set }
  var galleryPhotoPaddingL    : CGFloat { get set }
  //BCMapView
  var mapViewHeight           : CGFloat { get set }
  var mapPaddingH             : CGFloat { get set }
  var mapPaddingT             : CGFloat { get set }
  var mapPaddingB             : CGFloat { get set }
  var gdMapViewWidth          : CGFloat { get set }
  var gdMapViewHeight         : CGFloat { get set }
}
