
import UIKit

enum GDScreenDevice: String {
  
  //1 - iPhone 5
  case iPhone5
  
  //2 - iPhone 7
  case iPhone7
  
  //3 - iPhone Plus
  case iPhonePlus
  
  //4 - iPhone X
  case iPhoneX
  
  //5 - iPhone Max
  case iPhoneMax
  
  case iPhone12
  
  //6 - iPhone 12Max
  case iPhone12Max
  
  static func get() -> GDScreenDevice {
    let width : CGFloat = UIScreen.main.bounds.width
    let height: CGFloat = UIScreen.main.bounds.height
    
    print(width, height)
    switch (width, height) {
      
      case (320, 568):
        return .iPhone5
        
      case (375, 667):
        return .iPhone7
        
      case (414, 736):
        return .iPhonePlus
        
      case (375, 812):
        return .iPhoneX
        
      case (390, 844):
        return .iPhone12
        
      case (414, 896):
        return .iPhoneMax
        
      case (428, 926):
        return .iPhone12Max
      
      default:
        return .iPhone7
    }
  }
}
